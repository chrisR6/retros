<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package finnmccools
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="newsletter">
			<div class="container">
				<div class="row">
					<div class="text-center col-lg-12">
						<h4>Stay retro forever</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<?php print do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-9 menu_container">
					<div class="menu_wrapper">
						<div class="menu">
							<?php
								wp_nav_menu( array(
									'theme_location' => 'footer',
									'menu_id'        => 'footer',
									'depth'	          => 2, // 1 = no dropdowns, 2 = with dropdowns.
									'container'       => 'div',
									'menu_class'      => 'navbar-nav ',
								) );
							?>
						</div>
					</div>
					<div class="socials">
						<?php get_template_part( 'template-parts/social' ); ?>
					</div>
				</div> 
				<div class="col-lg-3 text-center text-lg-right logo">
					<div class="logo__container">
						<img src="<?php print get_template_directory_uri(); ?>/dist/img/logo.png" alt="Retros Logo" title="Retros Logo" />
					</div>
					<div class="copy text-center text-lg-right">
						<a href="https://ruby6.com.au" target="_blank" rel="noopener,noreferrer">WEBSITE BY RUBY6</a>
					</div>
				</div>
			</div>
		</div>
		
	</footer><!-- #colophon -->

	<div class="fullscreen-menu">
		<div class="header">
			<a class="d-inline-block d-lg-none mobile navbar-brand" href="<?php print home_url(); ?>">
				<img src="<?php print get_template_directory_uri(); ?>/dist/img/logo.png" alt="Retros Logo" title="Retros Logo" />
			</a>
			<button class="close navbar-toggler" type="button">
				<div class="close">X</div>
			</button>
		</div>
		<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'menu-1',
				'depth'	          => 3, // 1 = no dropdowns, 2 = with dropdowns.
				'container'       => 'div',
				'container_id'	  => 'headerNav',
				'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
				'walker'          => new WP_Bootstrap_Navwalker(),
			) );
		?>
				<?php get_template_part( 'template-parts/social' ); ?>
	</div>
</div><!-- #page -->
<script>var theme_url = "<?php print get_template_directory_uri(); ?>";</script>
<?php wp_footer(); ?>
<style>html { margin: 0 !important; }</style>
</body>
</html>
