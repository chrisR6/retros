if (typeof add_mgl_validation !== 'function'){
function add_mgl_validation(formID,functionRef,theFunction){
if (typeof MGL_VALIDATION_FUNCTIONS == 'undefined')
MGL_VALIDATION_FUNCTIONS = new Array(); 

if (typeof formID == 'undefined' || formID == '')
return false;
if (typeof functionRef == 'undefined' || functionRef == '')
return false;
if (typeof theFunction !== 'function')
return false;
if (typeof MGL_VALIDATION_FUNCTIONS['' + formID] == 'undefined'){
MGL_VALIDATION_FUNCTIONS['' + formID] = [];
MGL_VALIDATION_FUNCTIONS['ref_' + formID] = [];
}
MGL_VALIDATION_FUNCTIONS['' + formID].push(theFunction);
MGL_VALIDATION_FUNCTIONS['ref_' + formID].push(functionRef);
return true;
}

}

//IE6 Compatibility 
 ie = (function(){
 
    var undef,
        v = 3,
        div = document.createElement('div'),
        all = div.getElementsByTagName('i');
 
    while (
        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
        all[0]
    );
 
    return v > 4 ? v : undef;
 
}());
       window.onload = function () {
        if (document.getElementById('PatronName')){
            if (document.getElementById('PatronName').value == 'First Name'){
                document.getElementById('PatronName').style.color = 'gray';
            }
        }
    if (document.getElementById('PatronSurname')){
            if (document.getElementById('PatronSurname').value == 'Last Name'){
                document.getElementById('PatronSurname').style.color = 'gray';
            }
        }}
if (typeof mgl_selectall !== 'function'){
            function mgl_selectall(el,form){
                if (el.checked){
                    var action = true;
                    }else{
                    var action = false;
                }
                var attributes = el.attributes;
                var form = document.getElementById(form);
                elements = form.elements;
                for (i=0;i<elements.length;i++){
                    if (elements[i].name == attributes.getNamedItem('rel').value + '[]')
                        elements[i].checked = action;
                    }
                
                return;
                }
        
        }
    if (typeof mgl_set_optout !== 'function'){
        function mgl_set_optout(checkbox){
            var found = false;
            for(i=0;i<checkbox.attributes.length;i++){
                var attr = checkbox.attributes[i];
                if (attr.localName == 'rel')
                    var found = attr.nodeValue;
            }
            var parent = checkbox.parentNode;
            var inputs = parent.getElementsByTagName("input");
            var hidden = false;
            for(i=0;i<inputs.length;i++){
                var inp = inputs[i];
                var attr = inp.attributes;
                var attr = attr.getNamedItem('type')
                if (attr.name == "type" && attr.value == 'hidden'){
                    hidden = inp;
                    break;
                }
            }
            if (checkbox.checked)
                hidden.value = 0;
            else
                hidden.value = 1;

            return;
        }
    }
    
    if (typeof MGLSubmit !== 'function'){
        function MGLSubmit(formID,button){
            if(typeof document.compatMode !== 'string' && document.compatMode !== 'BackCompat'){
                mgl_warn('IE Compatibility mode detected');
                return;
            }                
            if (typeof formID !== 'undefined' && typeof document.getElementById(formID) !== null && document.getElementById(formID).onsubmit()){
                document.getElementById(formID).submit();
            }else if(typeof formID == 'undefined' && typeof button !== 'undefined' && typeof button.form !== 'undefined' && button.form.onsubmit()){
          		button.form.submit();
      		}else
      			mgl_warn('Could not find the form to be submitted, or a validation error occured');
        }
    }
  if(typeof $_GET !== 'function') {
    function $_GET(param)
{
   param = param.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");
   var regexS = "[\?&]" + param + "=([^&#]*)";
   var regex = new RegExp(regexS);
   var results = regex.exec(window.location.href);
	
   if(results == null)
      return false;
   else
      return results[1];
}
    }
  
  if(typeof clickclear !== 'function') {     
    function clickclear(thisfield, defaulttext) {
        if (thisfield.value == defaulttext) {
            thisfield.value = "";
            thisfield.style.color = "";
        }
    }
 }
if(typeof clickrecall !== 'function') {
    function clickrecall(thisfield, defaulttext) {
        if (thisfield.value == "") {
            thisfield.value = defaulttext;
            thisfield.style.color = "gray";
        }
    }
}
if(typeof mgl_console !== 'function') {
    function mgl_console(message){
        if (typeof console != 'undefined') {
            console.log(message);
            }
        }
    }
if(typeof mgl_info !== 'function'){
	function mgl_info(message){
		if (typeof console != 'undefined' && typeof console.info == 'function'){
			console.info(message);
			}else {
				mgl_console(message);
			}
		}
	}
if(typeof mgl_warn !== 'function'){
	function mgl_warn(message){
		if (typeof console != 'undefined' && typeof console.warn == 'function'){
			console.warn(message);
			}else {
				mgl_console(message);
			}
		}
	}
if(typeof mgl_error !== 'function'){
	function mgl_error(message){
		if (typeof console != 'undefined' && typeof console.error == 'function'){
			console.error(message);
			}else {
				mgl_console(message);
			}
		}
	}
    mgl_info('MyGuestlist.com Generated Form');
    mgl_info('This is MyGuestlist Form mf5ac2ca22d3be8');
if(typeof mgl_validator_mf5ac2ca22d3be8 !== 'function') {
    
        mgl_info('Using MyGuestlist form validation function');
        if (typeof mgl_validator_mf5ac2ca22d3be8 !== 'function'){
function mgl_validator_mf5ac2ca22d3be8(formID){
	elements = document.getElementById(formID).elements;
	form_parent = document.getElementById(formID);
	error = 0;
    checkbox  = 0;
	for(i=0;i<elements.length;i++)
	{
	element = elements[i];
	if(element.getAttribute("rel") == "required")
	{
	   if (element.type == "checkbox"){
	       if (!element.checked){
	           element.style.border = "2px solid red";
               checkbox = 1;
               error = 1;
	       }
	   }
       
       if ((document.getElementById('PatronName')) && (element.id == "PatronName") && (element.value == "First Name")){
            error = 1;
            element.style.border = "2px solid red";
       }
        else if ((document.getElementById('PatronSurname')) && (element.id == "PatronSurname") && (element.value == "Last Name")){
            error = 1;
            element.style.border = "2px solid red";
       }
	else if(element.value == "" || (element.value.length < 1 && element.value == 0 && element.getAttribute('mgltype') !== 'time') || element.value == 'Year') {
	   element.style.border = "2px solid red";
	   error = 1;
	}

       else if (element.getAttribute("rel") == "email"){
        //check this for valid email address
       if ((element.value.indexOf('@') < 0) || ((element.value.indexOf('.') < 0))){
            element.style.border = "2px solid red";
            error = 1;
       }
       else {
	       element.style.border = "";
	   }
        }
       
	else {
	   element.style.border = "";
	}
	}

       else if (element.getAttribute("rel") == "requiredAttendees")
       {
            if (element.name == "Attendees" && !document.getElementById('requiredAttendeeCheck').checked && (element.value == '' || element.value.length < 2)){
                element.style.border = "2px solid red";
                error = 1;
            }
            else if (element.name == "numberOfGuests" && document.getElementById('requiredAttendeeCheck').checked && (element.value == '' || element.value.length < 1)){
                element.style.border = "2px solid red";
                error = 1;
            }
            else if ((element.name == "Attendees" && document.getElementById('requiredAttendeeCheck').checked) || (element.name == "Attendees" && element.value.length > 2)){
                element.style.border = "";
            }
            else if ((element.name == "numberOfGuests" && !document.getElementById('requiredAttendeeCheck').checked) || (element.name == "numberOfGuests" && element.value.length > 0)){
                element.style.border = "";
            }
       }
       
    else if (element.getAttribute("rel") == "email"){
	   //check this for valid email address
       if ((element.value.indexOf('@') < 0) || ((element.value.indexOf('.') < 0))){
            element.style.border = "2px solid red";
            error = 1;
       }
       else {
	       element.style.border = "";
	   }
	}
	
	if (element.name == 'PatronMobile') {
		if (/^0+$/.test(element.value.replace(/\D*/g, ''))) {
			error = 1;
			element.style.border = '2px solid red';
		}
	}
	
	}
	
    if (getElementsByClassName('errordisplay',form_parent).length > 0){
        getElementsByClassName('errordisplay',form_parent)[0].style.color = '#F00';
           
    }
    
    
    if (typeof mgl_fb_app == 'function'){
        mgl_fb_app();
    }
    //Custom Form Validation Hook

if(typeof MGL_VALIDATION_FUNCTIONS !== 'undefined' && typeof MGL_VALIDATION_FUNCTIONS['mf5ac2ca22d3be8'] !== 'undefined' && MGL_VALIDATION_FUNCTIONS['mf5ac2ca22d3be8'].length > 0){
    			//Loop through each form validation function for this form
    			for (i in MGL_VALIDATION_FUNCTIONS['mf5ac2ca22d3be8']){
    				//get the function
					var f = MGL_VALIDATION_FUNCTIONS['mf5ac2ca22d3be8'][i];
					var thistry = f();
					if (!thistry){
						error = true;
            			mgl_warn('Your extended form validator function with ID ' + MGL_VALIDATION_FUNCTIONS["ref_mf5ac2ca22d3be8"][i] + ' returned an error');
   					}else
   						mgl_info('Extended form validator function ' + MGL_VALIDATION_FUNCTIONS["ref_mf5ac2ca22d3be8"][i] + ' returned no errors');
   				}
   			}


	if (checkbox){
	   if (getElementsByClassName('errordisplay',form_parent).length > 0){
            getElementsByClassName('errordisplay',form_parent)[0].innerHTML = "<strong>Please accept the Terms and Conditions</strong>";
        }
        return false;
    }
    else if(error) {
        if (getElementsByClassName('errordisplay',form_parent).length > 0){
	       getElementsByClassName('errordisplay',form_parent)[0].innerHTML = "<strong>Please make sure that all required fields have been completed</strong>";
        }
	return false;
	}
	else
	return true;
    }
	}
}
function getElementsByClassName(classname, node) { if (!node) { node = document.getElementsByTagName('body')[0]; } var a = [], re = new RegExp('\\b' + classname + '\\b'); els = node.getElementsByTagName('*'); for (var i = 0, j = els.length; i < j; i++) { if ( re.test(els[i].className) ) { a.push(els[i]); } } return a; }
if (typeof mgl_jquery_ready !== 'function')
	function mgl_jquery_ready(callback){
		if (typeof MGLJQUERY == 'undefined'){
		window.setTimeout(function(){
		mgl_jquery_ready(callback);
		},300);
		}
	else{
		return MGLJQUERY(document).ready(function(){
			callback(MGLJQUERY);
		})
	}
	}

	var MGL_USE_DOLLAR = false;
	function mgl_jquery_ui_mf5ac2ca22d3be8(){
if (typeof ie !== 'undefined' && ie < 7)
 return;

		if (typeof MGLJQUERY.ui == 'undefined' || typeof MGLJQUERY.ui.version == 'undefined' || MGLJQUERY.ui.version == 'undefined'){
		//load jquery ui
		 mgl_info('Loading jQuery UI from MyGuestlist');
		mgl_poll_jquery_ui_mf5ac2ca22d3be8(0);
		 return;
	}
	else{
		mgl_info('jQuery UI ' + MGLJQUERY.ui.version + ' detected');
		mgl_info('This form will use the theme from your own installation of jQuery UI');

	if (typeof MGLJQUERY().datepicker !== 'function' || typeof MGLJQUERY().button !== 'function'){
		mgl_warn('You do not have the jQuery UI Datepicker widget or button element, MyGuestlist will include our version of jQuery UI for you');
mgl_poll_jquery_ui_mf5ac2ca22d3be8(0);
	}else{
		mgl_info('You have the jQuery UI Datepicker widget');
 		setTimeout('mgl_jquerify_mf5ac2ca22d3be8()',2000);
}
}
}//end mgl_jquery_ui_mf5ac2ca22d3be8()

window.setTimeout(function(){
	mgl_jquery_ready(function(){
		mgl_jquery_ui_mf5ac2ca22d3be8();
	});
}, 800);
	function mgl_poll_jquery_ui_mf5ac2ca22d3be8(time_elapsed){
		if (time_elapsed < 1){
			if (document.getElementById('MGLIncludeJqueryUI') == null){
				var script = document.createElement('script')
  script.setAttribute("type","text/javascript")
  script.setAttribute("src", "https://cdn.myguestlist.com/mgl/lib/forms/jqueryUI.js")
  script.setAttribute("id", "MGLIncludeJqueryUI")
  if (typeof script!="undefined")
  document.getElementsByTagName("body")[0].appendChild(script)
		}
			var style = document.createElement('link')
  style.setAttribute("type","text/css")
  style.setAttribute("rel","stylesheet")
  style.setAttribute("href", "//ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/smoothness/jquery-ui.css")
  if (typeof style!="undefined")
  document.getElementsByTagName("head")[0].appendChild(style)
			setTimeout('mgl_poll_jquery_ui_mf5ac2ca22d3be8()',500);
 return; 
		}
else{
			if (time_elapsed > 20000) return;
			if (typeof MGLJQUERY().datepicker !== 'function') setTimeout('mgl_poll_jquery_ui_mf5ac2ca22d3be8()',time_elapsed + 200);
			else return mgl_jquerify_mf5ac2ca22d3be8();

}
}

function mgl_jquery(){
if (typeof ie !== 'undefined' && ie < 7)
 return;

mgl_info('Loading jQuery from Google CDN')
if (typeof $ !== 'null' && typeof $ == 'function' && typeof $() == 'function' && $().jquery){
	MGL_USE_DOLLAR = true;
}
if (typeof jQuery == 'function') current_jquery = jQuery.noConflict();
else current_jquery = false;
if(window.MooTools){
mgl_warn('Warning: MooTools ' + window.MooTools.version + ' detected.');}
 if(typeof Prototype != 'undefined'){
mgl_warn('Prototype detected. jQuery compatibility has not been tested with this');
 }
if (MGL_USE_DOLLAR || typeof $ == 'undefined') $ = current_jquery;
if (document.getElementById('MGLJqueryInclude') == null){
var script = document.createElement('script')
  script.setAttribute("type","text/javascript")
  script.setAttribute("id","MGLJqueryInclude")
  script.setAttribute("src", "https://cdn.myguestlist.com/mgl/lib/forms/jquery.1.6.1.min.js")
  if (typeof script!="undefined")
  document.getElementsByTagName("body")[0].appendChild(script)
}
mgl_poll_jquery(0);
}
function mgl_poll_jquery(time_elapsed){
//Polls for new jQuery version

if (typeof jQuery == 'function' && typeof current_jquery == 'function'){
if (jQuery().jquery == current_jquery().jquery && time_elapsed < 20000){
    setTimeout('mgl_poll_jquery(' + (time_elapsed + 200) + ')',200);
    return;
}
}else if (typeof jQuery !== 'function' && time_elapsed < 20000){
setTimeout('mgl_poll_jquery(' + (time_elapsed + 200) + ')',200);
return;
}
else if (time_elapsed > 20000){
mgl_error(time_elapsed + ' timeout loading new jQuery');
return false;
}
if (typeof MGLJQUERY == 'function')
	return MGLJQUERY;
MGLJQUERY = $.noConflict();
mgl_info('jQuery ' + MGLJQUERY().jquery + ' loaded');
if (MGL_USE_DOLLAR) $ = current_jquery;
if (typeof current_jquery == "function" && current_jquery().jquery) jQuery = current_jquery;
return;
}


if(typeof jQuery != 'function'){
mgl_info('jQuery not detected. Attempting to load it');
setTimeout('mgl_jquery()',1000)}
else{
mgl_info('jQuery ' + jQuery().jquery + ' detected');

if (typeof jQuery == 'function' && parseFloat(jQuery().jquery) >= 1.3 && parseFloat(jQuery().jquery) < 1.9){
mgl_info('jQuery ' + parseFloat(jQuery().jquery) + ' is OK')
MGLJQUERY = jQuery;
 mgl_jquery_ui_mf5ac2ca22d3be8();
}else{
mgl_warn('Note: Your version of jQuery is not compatible with the MyGuestlist calendar widget.')
setTimeout('mgl_jquery()',1000)
}
}function mgl_date_mf5ac2ca22d3be8_Date(date){
if (date.getDay() == 0 || date.getDay() == 1 || date.getDay() == 2 || date.getDay() == 3 || date.getDay() == 4)
{ return [false]; }

 return [true]; 
}//end function


function mgl_jquerify_mf5ac2ca22d3be8(){
if (MGLJQUERY('button[type=\'submit\']', '#mf5ac2ca22d3be8').length > 0 && typeof MGLJQUERY('button[type=\'submit\']', '#mf5ac2ca22d3be8').button == 'function'){
	MGLJQUERY('button[type=\'submit\']', '#mf5ac2ca22d3be8').button();
}

MGLJQUERY('[mglrel="date"]','#mf5ac2ca22d3be8').each(function(){
		if (MGLJQUERY(this).attr('name') !== 'DOB_yyyy'){
		var name = MGLJQUERY(this).attr('name');
 name = name.replace('_yyyy','');
		var required = MGLJQUERY(this).attr('rel') == 'required' ? true : false;
		if (MGLJQUERY('option',this).length > 0){
			if (MGLJQUERY("option[value='2022']").length > 0){
					 maxyear = 2;
}
		else{
		maxyear = 1;}
}
	else if(MGLJQUERY('option',this).length < 1){ maxyear = false; }
	else{ maxyear = 1; }
			var parent = MGLJQUERY(this).parent('div');
			MGLJQUERY('[name="' + name + '_dd"],[name="' + name + '_mm"],[name="' + name + '_yyyy"]',parent).remove();
			var input = MGLJQUERY('mf5ac2ca22d3be8_' + name);
				input = MGLJQUERY('#mf5ac2ca22d3be8_' + name);
			input.css('padding-left',16).css('display','');
 			 input.css('background-image','url(//www.myguestlist.com.au/mgl/images/formgen/date.png)')
 			 input.css('background-repeat','no-repeat');
			input.attr('readonly','readonly')
			if (required) input.attr('rel','required');
			eval('MGLJQUERY(input).datepicker({ dateFormat: \'dd M yy\',changeYear: true, minDate: 0, beforeShowDay: mgl_date_mf5ac2ca22d3be8_' + name + ' })');
			eval('MGLJQUERY(input).closest(\'.MGLRow\').append(\'<input name="mgldaterealval__\' + MGLJQUERY(input).attr(\'name\') + \'" type="hidden" style="display:none;">\')');
			if (maxyear){ MGLJQUERY(input).datepicker('option','maxDate','+' + maxyear + 'y');
}
 setTimeout('MGL_set_datepicker_options_mf5ac2ca22d3be8()',1000);
}
})
}
function MGL_set_datepicker_options_mf5ac2ca22d3be8(){

        MGLJQUERY('[name^="mgldaterealval__"]' , '#mf5ac2ca22d3be8').each(function() {
            var name = MGLJQUERY(this).attr('name').substring(16);
            MGLJQUERY('[id="mf5ac2ca22d3be8_'+name+'"]').datepicker('option', 'altFormat', 'dd/mm/yy');
            MGLJQUERY('[id="mf5ac2ca22d3be8_'+name+'"]').datepicker('option', 'altField', MGLJQUERY('[name="mgldaterealval__'+name+'"]' , '#mf5ac2ca22d3be8'));
        });
        
 MGLJQUERY('#mf5ac2ca22d3be8_Date').datepicker('option','numberOfMonths',3)


var cutoff_reference_mf5ac2ca22d3be8 = 1605427200;
function isPastCutoff_mf5ac2ca22d3be8()
    {
  var current_time = new Date();
  //Loop over the date reference
  var resolved = false;
  //Add 24 hours until we are at today
  var working_timestamp = cutoff_reference_mf5ac2ca22d3be8;
  var working_date = new Date(working_timestamp * 1000);
  while (!resolved) {
   working_timestamp = working_timestamp + 86400 //Plus 24 hours
    working_date = new Date(working_timestamp * 1000);
    if (working_date.getMonth() == current_time.getMonth() && working_date.getDate() == current_time.getDate()) {
      //Set the working date!
      resolved = true;
      break;
    }
  }
  mgl_info('Current time');
  mgl_info(current_time);
  mgl_info('Cutoff time');
  mgl_info(working_date);
  if (current_time.valueOf() < working_date.valueOf()) {
    return false;
  } else
    return true;
}
if (isPastCutoff_mf5ac2ca22d3be8()){
	 MGLJQUERY('#mf5ac2ca22d3be8_Date').datepicker('option','minDate','+1d')
mgl_info('The cutoff time for today has been reached, cannot select todays date');
}
else{
	 MGLJQUERY('#mf5ac2ca22d3be8_Date').datepicker('option','minDate',new Date())
mgl_info('The cutoff time is in the future, so today can be selected');
}



if (typeof reload_submission == "function"){
reload_submission();
}
}
add_mgl_validation("mf5ac2ca22d3be8","setCutOffTime",function(){
   var selectedDate = MGLJQUERY("#mf5ac2ca22d3be8_Date").datepicker("getDate");
   if(!selectedDate)
     return false;
   var now = new Date();
  
  if(now.getDate() == selectedDate.getDate() && now.getMonth() == selectedDate.getMonth() && now.getFullYear() == selectedDate.getFullYear) {
    if(now.getHours() >= 20) {
      alert("Error: Cut-off time for tonights guestlist is 8pm.");
      return false;
    }
    
  }
  
  return true;
});




function getAllUrlParams(url) {
  var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
  var obj = {};
  if (queryString) {
    queryString = queryString.split('#')[0];
    var arr = queryString.split('&');
    for (var i = 0; i < arr.length; i++) {
      var a = arr[i].split('=');
      var paramName = a[0];
      var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];
      paramName = paramName.toLowerCase();
      if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();
      if (paramName.match(/\[(\d+)?\]$/)) {
        var key = paramName.replace(/\[(\d+)?\]/, '');
        if (!obj[key]) obj[key] = [];
        if (paramName.match(/\[\d+\]$/)) {
          var index = /\[(\d+)\]/.exec(paramName)[1];
          obj[key][index] = paramValue;
        } else {
          obj[key].push(paramValue);
        }
      } else {
        if (!obj[paramName]) {
          obj[paramName] = paramValue;
        } else if (obj[paramName] && typeof obj[paramName] === 'string'){
          obj[paramName] = [obj[paramName]];
          obj[paramName].push(paramValue);
        } else {
          obj[paramName].push(paramValue);
        }
      }
    }
  }

  return obj;
}

if (getAllUrlParams().ls == 'true') {
  console.log("ls == true");
  var link = document.createElement( "link" );
  link.type = "text/css";
  link.rel = "stylesheet";
  link.href = "https://retros.apex-ads.com.au/goldcoast/wp-content/themes/html5-child-retros/iframe.css";
  document.getElementsByTagName( "head" )[0].appendChild( link );
}



ie = (function(){
 
    var undef,
        v = 3,
        div = document.createElement('div'),
        all = div.getElementsByTagName('i');
 
    while (
        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
        all[0]
    );
 
    return v > 4 ? v : undef;
 
}());
if (typeof ie == 'undefined' || ie > 6){
if ($_GET('formSubmitted') == 'mf5ac2ca22d3be8'){
    document.write("<div class=\"MGLSuccessMessage\">https://hallmarkgroupaus.wixsite.com/retros/guest-list-confirmation</div>");
    }else if ($_GET('formEmailVerification') == 'mf5ac2ca22d3be8'){
    document.write("Thanks! A confirmation e-mail with instructions has been sent to you. Please check your e-mail inbox and the junk mail folder...just in case.");
    }else{
    if ($_GET('formAllowedError') == 'mf5ac2ca22d3be8'){
 window.setTimeout(function(){
 var els = document.getElementsByClassName('MGLFormAllowedError', document.getElementById('mf5ac2ca22d3be8'));
for(i=0;i<els.length;i++){
els[i].style.display = 'block'
}
}, 1500);
 } 
document.write("<style>\ndiv.MGLLabel{\n            /*width: 35%;\n            float: left;*/\n            font-weight: bold;\n            }\n            div.MGLField{\n                /*margin-left: 5%;\n                float:left;*/\n            }\n            div.MGLRow{\n                clear: both;\n                padding-bottom: 25px;\n            }\n            div.MGLField input{\n                padding: 0;\n                }\n            div.MGLSeperator{\n                border-bottom: 1px solid gray;\n                padding-bottom: 10px;\n                margin-bottom: 10px;\n            }\n            div.MGLText{\n                padding-bottom: 10px;\n                margin-bottom: 10px;\n            }\n            \n            span.MGLTitle{\n                font-weight: bold;\n            }\n            \n            .ui-datepicker{\n                font-size: 10px !important;\n            }\n            \n            .MGLFormAllowedError,.MGLFormVerification{\n            display: none;\n            color: #F00;\n            }.MGLLabel {\n     color: white;\n    font-family: sans-serif;\n}\ndiv.MGLLabel {\n    font-weight: 300;\n      font-family: sans-serif;\n}\ninput[type=\"submit\" i] {\n      border-color:#333;\n  background: #000000;\n  color: #ffffff;\nborder-radius: 0px;\npadding:       8px 20px;\ndisplay:       inline-block;\nfont:          normal normal 14px/1 \"Open Sans\", sans-serif;\ntext-align:    center;\n}\ninput[type=\"submit\" i]:focus {\n  background:    #ffffff;\nborder:        1px solid #000000;\n  color: #000;\n}\ninput[type=\"submit\" i]:hover {\nbackground:    #ffffff;\nborder:        1px solid #000000;\n    color: #000;\n}\n</style>\n<form action=\"https://myguestlist.com/mgl/formreceiver.php?type=guestlist\" method=\"post\" id=\"mf5ac2ca22d3be8\" onsubmit=\"return mgl_validator_mf5ac2ca22d3be8('mf5ac2ca22d3be8')\">\n<input type=\"hidden\" name=\"formID\" value=\"mf5ac2ca22d3be8\" />\n<input type=\"hidden\" name=\"isGuestlist\" value=\"1\" />\n<div class=\"MGLRow MGLFormAllowedError\" id=\"\">Sorry - This form is not available for everyone</div><div class=\"MGLRow MGLFormVerification\">Thanks! A confirmation email with instructions has been sent to you. Please check your email inbox and the junk mail folder...just incase.</div><div class=\"MGLRow\">\n<div class=\"MGLLabel\">First Name*</div>\n<div class=\"MGLField\"><input type=\"text\" name=\"PatronName\" value=\"\" rel=\"required\" id=\"mf5ac2ca22d3be8_PatronName\" /></div>\n</div>\n<div class=\"MGLRow\">\n<div class=\"MGLLabel\">Last Name*</div>\n<div class=\"MGLField\"><input type=\"text\" name=\"PatronSurname\" value=\"\" rel=\"required\" id=\"mf5ac2ca22d3be8_PatronSurname\" /></div>\n</div>\n<div class=\"MGLRow\">\n<div class=\"MGLLabel\">E-mail*</div>\n<div class=\"MGLField\"><input type=\"text\" name=\"PatronEmail\" value=\"\" rel=\"email\" id=\"mf5ac2ca22d3be8_PatronEmail\" /></div>\n</div>\n<div class=\"MGLRow\">\n<div class=\"MGLLabel\">Mobile*</div>\n<div class=\"MGLField\"><input type=\"text\" name=\"PatronMobile\" value=\"\" rel=\"required\" id=\"mf5ac2ca22d3be8_PatronMobile\" /></div>\n</div>\n<div class=\"MGLRow\" style=\"display:none;\">\n<div class=\"MGLLabel\">Guestlist Name</div>\n<div class=\"MGLField\"><input type=\"text\" name=\"GuestListName\" value=\"\"  id=\"mf5ac2ca22d3be8_GuestListName\" /></div>\n</div>\n<div class=\"MGLRow\">\n<div class=\"MGLLabel\">Date*</div>\n<div class=\"MGLField\"><select name=\"Date_dd\" rel=\"required\" id=\"Date_dd\">\n<option value=\"\" selected=\"selected\">Day</option>\n<option value=\"1\">01</option>\n<option value=\"2\">02</option>\n<option value=\"3\">03</option>\n<option value=\"4\">04</option>\n<option value=\"5\">05</option>\n<option value=\"6\">06</option>\n<option value=\"7\">07</option>\n<option value=\"8\">08</option>\n<option value=\"9\">09</option>\n<option value=\"10\">10</option>\n<option value=\"11\">11</option>\n<option value=\"12\">12</option>\n<option value=\"13\">13</option>\n<option value=\"14\">14</option>\n<option value=\"15\">15</option>\n<option value=\"16\">16</option>\n<option value=\"17\">17</option>\n<option value=\"18\">18</option>\n<option value=\"19\">19</option>\n<option value=\"20\">20</option>\n<option value=\"21\">21</option>\n<option value=\"22\">22</option>\n<option value=\"23\">23</option>\n<option value=\"24\">24</option>\n<option value=\"25\">25</option>\n<option value=\"26\">26</option>\n<option value=\"27\">27</option>\n<option value=\"28\">28</option>\n<option value=\"29\">29</option>\n<option value=\"30\">30</option>\n<option value=\"31\">31</option>\n</select> <select name=\"Date_mm\" rel=\"required\" id=\"Date_mm\">\n<option value=\"\" selected=\"selected\">Month</option>\n<option value=\"1\">Jan</option>\n<option value=\"2\">Feb</option>\n<option value=\"3\">Mar</option>\n<option value=\"4\">Apr</option>\n<option value=\"5\">May</option>\n<option value=\"6\">Jun</option>\n<option value=\"7\">Jul</option>\n<option value=\"8\">Aug</option>\n<option value=\"9\">Sep</option>\n<option value=\"10\">Oct</option>\n<option value=\"11\">Nov</option>\n<option value=\"12\">Dec</option>\n</select> <input type=\"text\" name=\"Date_yyyy\" value=\"Year\" size=\"4\" maxlength=\"4\" onfocus=\"clickclear(this, 'Year')\" onblur=\"clickrecall(this, 'Year')\" id=\"MGLDate\" rel=\"required\" mglrel=\"date\" id=\"Date_yyyy\" />\n<input type=\"text\" style=\"display:none;\" name=\"Date\" id=\"mf5ac2ca22d3be8_Date\" /></div>\n</div>\n<div class=\"MGLRow\">\n<div class=\"MGLLabel\">Time of Entry*</div>\n<div class=\"MGLField\"><select name=\"TimeOfEntry\" rel=\"required\">\n<option value=\"\" selected=\"selected\"></option>\n<option value=\"9PM (FRIDAY)\">9PM (FRIDAY)</option>\n<option value=\"9:30PM (FRIDAY)\">9:30PM (FRIDAY)</option>\n<option value=\"10PM (FRIDAY)\">10PM (FRIDAY)</option>\n<option value=\"8PM (SATURDAY)\">8PM (SATURDAY)</option>\n<option value=\"8:30PM (SATURDAY)\">8:30PM (SATURDAY)</option>\n<option value=\"9PM (SATURDAY)\">9PM (SATURDAY)</option>\n<option value=\"9:30PM (SATURDAY)\">9:30PM (SATURDAY)</option>\n<option value=\"10PM (SATURDAY)\">10PM (SATURDAY)</option>\n</select></div>\n</div>\n<div class=\"MGLRow\" style=\"display:none;\">\n<div class=\"MGLLabel\">Time</div>\n<div class=\"MGLField\"><select name=\"Time[]\"  mgltype=\"time\">\n<option value=\"\">HH</option>\n<option value=\"0\">00</option>\n<option value=\"1\">01</option>\n<option value=\"2\">02</option>\n<option value=\"3\">03</option>\n<option value=\"4\">04</option>\n<option value=\"5\">05</option>\n<option value=\"6\">06</option>\n<option value=\"7\">07</option>\n<option value=\"8\">08</option>\n<option value=\"9\">09</option>\n<option value=\"10\">10</option>\n<option value=\"11\">11</option>\n<option value=\"12\">12</option>\n<option value=\"13\">13</option>\n<option value=\"14\">14</option>\n<option value=\"15\">15</option>\n<option value=\"16\">16</option>\n<option value=\"17\">17</option>\n<option value=\"18\">18</option>\n<option value=\"19\">19</option>\n<option value=\"20\">20</option>\n<option value=\"21\" selected=\"selected\">21</option>\n<option value=\"22\">22</option>\n<option value=\"23\">23</option>\n</select>\n<select name=\"Time[]\"  mgltype=\"time\">\n<option value=\"\">MM</option>\n<option value=\"00\" selected=\"selected\">00</option>\n<option value=\"01\">01</option>\n<option value=\"02\">02</option>\n<option value=\"03\">03</option>\n<option value=\"04\">04</option>\n<option value=\"05\">05</option>\n<option value=\"06\">06</option>\n<option value=\"07\">07</option>\n<option value=\"08\">08</option>\n<option value=\"09\">09</option>\n<option value=\"10\">10</option>\n<option value=\"11\">11</option>\n<option value=\"12\">12</option>\n<option value=\"13\">13</option>\n<option value=\"14\">14</option>\n<option value=\"15\">15</option>\n<option value=\"16\">16</option>\n<option value=\"17\">17</option>\n<option value=\"18\">18</option>\n<option value=\"19\">19</option>\n<option value=\"20\">20</option>\n<option value=\"21\">21</option>\n<option value=\"22\">22</option>\n<option value=\"23\">23</option>\n<option value=\"24\">24</option>\n<option value=\"25\">25</option>\n<option value=\"26\">26</option>\n<option value=\"27\">27</option>\n<option value=\"28\">28</option>\n<option value=\"29\">29</option>\n<option value=\"30\">30</option>\n<option value=\"31\">31</option>\n<option value=\"32\">32</option>\n<option value=\"33\">33</option>\n<option value=\"34\">34</option>\n<option value=\"35\">35</option>\n<option value=\"36\">36</option>\n<option value=\"37\">37</option>\n<option value=\"38\">38</option>\n<option value=\"39\">39</option>\n<option value=\"40\">40</option>\n<option value=\"41\">41</option>\n<option value=\"42\">42</option>\n<option value=\"43\">43</option>\n<option value=\"44\">44</option>\n<option value=\"45\">45</option>\n<option value=\"46\">46</option>\n<option value=\"47\">47</option>\n<option value=\"48\">48</option>\n<option value=\"49\">49</option>\n<option value=\"50\">50</option>\n<option value=\"51\">51</option>\n<option value=\"52\">52</option>\n<option value=\"53\">53</option>\n<option value=\"54\">54</option>\n<option value=\"55\">55</option>\n<option value=\"56\">56</option>\n<option value=\"57\">57</option>\n<option value=\"58\">58</option>\n</select> </div>\n</div>\n<div class=\"MGLRow\" style=\"display:none;\">\n<div class=\"MGLLabel\">Type</div>\n<div class=\"MGLField\"><select name=\"Type\">\n<option value=\"\"></option>\n<option value=\"Birthday\">Birthday</option>\n<option value=\"Hens Night\">Hens Night</option>\n<option value=\"Going Away Party\">Going Away Party</option>\n<option value=\"After Work Drinks\">After Work Drinks</option>\n<option value=\"Corporate Function\">Corporate Function</option>\n<option value=\"Other\">Other</option>\n<option value=\"10PM (SATURDAY)\">10PM (SATURDAY)</option>\n</select></div>\n</div>\n<div class=\"MGLRow\">\n<div class=\"MGLLabel\">Attendees</div>\n<div class=\"MGLField\"><div id=\"MGLattendeeContainer\"><textarea name=\"Attendees\" cols=\"30\" rows=\"12\" id=\"mf5ac2ca22d3be8_Attendees\"></textarea><br /><small>One name per line</small></div><span style=\"display:none\"><input type=\"checkbox\" onclick=\"mgl_showApprox(this);\" name=\"createOpenList\" />Create an open list for me</span><div id=\"MGLnumberContainer\" style=\"display:none;\">Approx Number of Guests? <input type=\"text\" name=\"numberOfGuests\" size=\"5\" /><br /></div><br /><br />\n</div>\n</div>\n<div class=\"MGLRow MGLText\">\n<span>All submissions must be received by 7pm on the day. Please arrive to the venue by 10pm. \nFurther terms and conditions may apply. Due to new Covid-19 legislations, we cannot guarantee entry.</span></div>\n<div class=\"MGLRow\"><br class=\"MGLSubmitBreak\" />\n<div id=\"MGLSubmit\" style=\"text-align:center;\"><button type=\"submit\" id=\"MGLSubmitButton\">Get on the Guest List!</button><br />\n<span style=\"text-align:center;display:block;position:relative;margin-left:auto;margin-right:auto;\" id=\"errordisplay\"></span>\n<div class=\"MGLPoweredBy\">Powered by <a href=\"http://www.myguestlist.com?utm_source=poweredby&utm_medium=forms&utm_campaign=Powered%2BBy%2BTracking\" target=\"_blank\">MyGuestlist</a></div>\n</div>\n</div>\n</form>\n")

}}