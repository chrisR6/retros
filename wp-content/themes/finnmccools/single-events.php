<?php
/**
 * The template for displaying event details
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package finnmccools
 */

get_header();
?>

	<main id="primary" class="site-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="event__details">
                        <div class="clover rotating clover--large"></div> 
                        <div class="clover rotating rotating--med clover--alt clover--med"></div>
                        <div class="clover rotating rotating--med clover--small"></div>
                        <div class="col-lg-2">
                            <div data-wow-delay="0.25s" class="wow fadeIn block block--header">
                                <div class="content">
                                <?php
                                $date = get_field('event_start_date');
                                // $theDate = date("l jS F", strtotime($date));
                                // $theStartTime = date("g:ia", strtotime($date));
                                $theDate = date("j ", strtotime($date));
                                $theMonth = date("M ", strtotime($date));
                                $theStartTime = date("g:ia", strtotime($date));
                                // $theEndTime = date("g:ia", strtotime(get_field('end_time', get_queried_object_id())));
                                ?>
                                <div class="block date"><?php print $theDate; ?></div>
                                <div class="block month"><?php print $theMonth; ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-10 event__content">
                            <div class="col-lg-7 offset-lg-1 event__content__container">
                                <h1 data-wow-delay="0.5s" class="wow fadeIn" ><?php the_title(); ?></h1>
                                <span data-wow-delay="0.75s"  class="wow fadeIn date"><?php print date("l jS F @ ga", strtotime('+10 hours',strtotime($date))); ?></span>
                                <span data-wow-delay="0.75s"  class="wow fadeIn venue"><?php print get_field("venue")->post_title; ?></span>
                                <div data-wow-delay="0.75s"  class="wow fadeIn content"><?php the_content(); ?></div>
                            </div>
                            <div class="col-lg-4 event__image">
                                <img data-wow-delay="1s"  class="wow fadeIn d-none d-lg-block " src="<?php print get_field("teaser_image"); ?>" />
                                <?php if( get_field("book_ticket_url") ): ?>
                                    <div data-wow-delay="1.25s"  class="wow fadeIn cta_button">
                                        <a class="button" target="_blank" href="<?php print get_field("book_ticket_url"); ?>">
                                            MORE INFO
                                        </a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="similar__events">
            <?php
                date_default_timezone_set('Australia/Brisbane');
                $date_now = current_time('Y-m-d H:i:s');
                $now = getStartTimeframe( -1 );
                $end = getEndTimeframe( -1 );
                $queryArgs = array(
                    'post_type'  => 'events',
                    'order'      => 'asc',
                    'orderby'    => 'meta_value',
                    'meta_key'   => 'event_start_date',
                    'category__in'   => wp_get_post_categories( get_the_ID() ),
                    'posts_per_page' => 3,
                    'post__not_in'   => array( get_the_ID() ),
                    'meta_query' => array(
                        'relation'    => 'AND',
                        array(
                            'relation'    => 'AND',
                            array(
                                'key'     => 'event_start_date',
                                'value'   => array($now, $end),
                                'compare' => 'BETWEEN',
                                'type'    => 'DATE'
                            ),
                            array(
                                'key'     => 'venue',
                                'value'   => get_field("venue")->ID,//'110',
                                'compare' => '=',
                            )
                        )
                    ),
                );
                $events = new WP_Query($queryArgs);
                // print_r($ss);
                if( $events->have_posts() ) { ?>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 section__title__container text-center">
                                    <h4 class="wow fadeIn section__title">You may be interested in these similar events</h4>
                                </div>
                            </div>
                        </div>
                        <div class="event_container justify-content-center">
                        <?php
                        $ii = 0;while( $events->have_posts() ):  $events->the_post(); 
                            $date = get_field('event_start_date');
                            $theDate = date("j ", strtotime($date));
                            $theMonth = date("M ", strtotime($date));
                            ?>
                            <a href="<?php the_permalink(); ?>" data-wow-delay="<?php print $ii * 0.25; ?>s" class="wow fadeIn event">
                                <div class="">
                                    <div class="event__image" style="background-image: url(<?php print get_field("event_image"); ?>)"></div>
                                    <div class="event__banner">
                                        <div class="event__date">
                                            <div class="block date"><?php print $theDate; ?></div>
                                            <div class="block month"><?php print $theMonth; ?></div>
                                        </div>
                                        <div class="event__info">
                                            <h3><?php the_title(); ?></h3>
                                            <span class="date"><?php print date("l @ ga", strtotime('+10 hours',strtotime($date))); ?></span>
                                            <span class="venue"><?php print get_field("venue")->post_title; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php 
                        $ii++;endwhile; ?>
                        </div>
                    <?php
                    wp_reset_postdata();
                    }
                    ?>
        </div>
		
        <div class="container">
            <div class="row">
                <div class="col-lg-12 section__title__container upcoming text-center">
                    <h4 class="wow fadeIn section__title">Upcoming Events</h4>
                </div>
            </div>
        </div>


        <div class="upcoming__events">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
            <?php
                date_default_timezone_set('Australia/Brisbane');
                $date_now = current_time('Y-m-d H:i:s');
                $now = getStartTimeframe( -1 );
                $end = getEndTimeframe( -1 );
                $eventId = get_the_ID();
                $queryArgs = array(
                    'post_type'  => 'events',
                    'order'      => 'asc',
                    'orderby'    => 'meta_value',
                    'meta_key'   => 'event_start_date',
                    // 'category__in'   => wp_get_post_categories( get_the_ID() ),
                    'posts_per_page' => 5,
                    'post__not_in'   => array( get_the_ID() ),
                    'meta_query' => array(
                        'relation'    => 'AND',
                        array(
                            'key'     => 'event_start_date',
                            'value'   => array($now, $end),
                            'compare' => 'BETWEEN',
                            'type'    => 'DATE'
                        ),
                        array(
                            'key'     => 'venue',
                            'value'   => get_field("venue")->ID,//'110',
                            'compare' => '=',
                        )
                    ),
                );
                $events = new WP_Query($queryArgs);
                // print_r($ss);
                if( $events->have_posts() ) {
                    $posts = 0; while( $events->have_posts() ):  $events->the_post(); 
                        if( get_the_ID() == $eventId ) {
                            continue;
                        }
                        print '<!-- DEBUG: ' . get_the_ID() . ' ::: ' . $eventId . ' -->';
                        $date = get_field('event_start_date');
                        $theDate = date("j ", strtotime($date));
                        $theMonth = date("M ", strtotime($date));
                        ?>
                            <div data-wow-delay="<?php print $posts * 0.25; ?>s" class="wow fadeIn carousel-item <?php print $posts == 0 ? "active": ""; ?>">
                                <a href="<?php the_permalink(); ?>" class="event">
                                    <img src="<?php print get_field("event_image", get_the_ID()); ?>" />
                                </a>
                            </div>
                <?php 
                    $posts++; endwhile;
                    wp_reset_postdata();
                } ?>
                </div>

                <ol class="carousel-indicators">
                    <?php for($ii = 0; $ii < $posts; $ii++): ?>
                        <li data-target="#carouselExampleIndicators" data-slide-to="<?php print $ii; ?>" class="<?php print $ii == 0 ? "active" : ""; ?>"></li>
                    <?php endfor; ?>
                </ol>
            <?php
            ?>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-lg-12 section__title__container back text-center">
                    <div class="wow fadeIn cta_button">
                        <a class="button" href="<?php print get_permalink(get_page_by_title(( get_field("venue")->ID == 204 ? "What's on Gold Coast" : "What's on Brisbane" ) )); ?>">
                            BACK TO WHAT'S ON
                        </a>
                    </div>
                </div>
            </div>
        </div>

	</main><!-- #main -->

<?php
get_footer();
