<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package finnmccools
 */

get_header();
?>

	<main id="primary" class="site-main">
		<div class="block_container layout--content_block">
			<section class="block block--content_block" style="padding-bottom: 65px;">
				<div class="container">
					<div class="clover rotating clover--large"></div> 
					<div class="clover rotating rotating--med clover--alt clover--med"></div>
					<div class="row">
						<div class="col-lg-12">
							<div class="item__inner">
								<div data-wow-delay="0.5s" class="content wow fadeIn">
									<div class="item__title">
										<h2>Page not found.</h2>
									</div>
									<div class="item__content">
										We couldn't find the page you were looking for, please click <a style="color: #085c2c;" href="<?php print home_url(); ?>">here</a> to return home.
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="clover rotating rotating--med clover--small"></div>
				</div>
			</section><!-- block.block--content_block-->
		</div>

	</main><!-- #main -->

<?php
get_footer();
