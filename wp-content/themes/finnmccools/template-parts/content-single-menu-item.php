<div class="menu__section seperator--<?php print get_sub_field("seperator_style"); ?>">

    <?php if(get_sub_field("section_image")): ?>
        <div class="section__image">
            <img src="<?php the_sub_field("section_image"); ?>" />
        </div>
    <?php endif; ?>

    <div class="section__title"><?php the_sub_field("section_title"); ?></div>

    <div class="section__items">
        <?php while( have_rows("items") ): the_row(); ?>
            <div class="menu__item <?php print get_sub_field("is_special") ? "menu__item--special":""; ?>">
                <?php if( get_sub_field("is_special") && get_sub_field("menu_item_image") ): ?>

                    <div class="header">
                        <img src="<?php print get_sub_field("menu_item_image"); ?>" />
                        <div class="content">
                            <span class="title"><?php the_sub_field("title"); ?>
                                <?php if( get_sub_field("price") ): ?>
                                    <span class="price"><?php the_sub_field("price"); ?></span>
                                <?php endif; ?>
                            </span>
                            <?php if( get_sub_field("variations") ): ?>
                                <div class="variations">
                                    <?php the_sub_field("variations"); ?>
                                </div>
                            <?php endif; ?>

                            <?php if( get_sub_field("description") ): ?>
                                <div class="description">
                                    <?php the_sub_field("description"); ?>
                                </div>
                            <?php endif; ?>

                            <?php if( get_sub_field("variations") ): ?>
                                <div class="additional_text">
                                    <?php the_sub_field("additional_text"); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>



                <?php else: ?>

                    <?php if( get_sub_field("title") || get_sub_field("price") ): ?>
                        <div class="header">
                            <span class="title"><?php the_sub_field("title"); ?></span>
                            <?php if( get_sub_field("price") ): ?>
                                <span class="price"><?php the_sub_field("price"); ?></span>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                    <?php if( get_sub_field("variations") ): ?>
                        <div class="variations">
                            <?php the_sub_field("variations"); ?>
                        </div>
                    <?php endif; ?>

                    <?php if( get_sub_field("description") ): ?>
                        <div class="description">
                            <?php the_sub_field("description"); ?>
                        </div>
                    <?php endif; ?>

                    <?php if( get_sub_field("variations") ): ?>
                        <div class="additional_text">
                            <?php the_sub_field("additional_text"); ?>
                        </div>
                    <?php endif; ?>

                <?php endif; ?>
                
                
            </div>
        <?php endwhile; ?>
    </div>

</div>