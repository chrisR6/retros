<?php 
if( is_home() && !is_front_page() ) {
    $pageId = get_option( 'page_for_posts' );
} else {
    $pageId = get_the_ID();
}

?>
<?php if (have_rows('blocks', $pageId)):  ?>
    <?php while (have_rows('blocks', $pageId)) : the_row(); ?>
    
        <div class="block_container layout--<?php print get_row_layout(); ?>">
            <?php get_template_part( 'template-parts/panels/panel', get_row_layout() ); ?>
        </div>
    <?php endwhile; // have_rows('panel') ?>
<?php endif; // have_rows('panel') ?>
