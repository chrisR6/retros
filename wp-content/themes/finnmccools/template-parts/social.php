<div class="social__container">
    <div class="social_wrap">
        <span class="title">BRISBANE</span>
        <?php if( get_field("facebook_url", "options") ) : ?>
            <a target="_blank" href="<?php print get_field("facebook_url", "options"); ?>" class="social__site site--facebook"></a>
        <?php endif; ?>
        <?php if( get_field("twitter_url", "options") ) : ?>
            <a target="_blank" href="<?php print get_field("twitter_url", "options"); ?>" class="social__site site--twitter"></a>
        <?php endif; ?>
        <?php if( get_field("instagram_url", "options") ) : ?>
            <a target="_blank" href="<?php print get_field("instagram_url", "options"); ?>" class="social__site site--instagram"></a>
        <?php endif; ?>
    </div>
    <div class="social_wrap">
        <span class="title">GOLD COAST</span>
        <?php if( get_field("gc_facebook_url", "options") ) : ?>
            <a target="_blank" href="<?php print get_field("gc_facebook_url", "options"); ?>" class="social__site site--facebook"></a>
        <?php endif; ?>
        <?php if( get_field("gc_twitter_url", "options") ) : ?>
            <a target="_blank" href="<?php print get_field("gc_twitter_url", "options"); ?>" class="social__site site--twitter"></a>
        <?php endif; ?>
        <?php if( get_field("gc_instagram_url", "options") ) : ?>
            <a target="_blank" href="<?php print get_field("gc_instagram_url", "options"); ?>" class="social__site site--instagram"></a>
        <?php endif; ?>
    </div>
</div>