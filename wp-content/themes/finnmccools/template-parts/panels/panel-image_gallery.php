<section class="block block--<?php print get_row_layout(); ?>">
    <div class="image__gallery">
        <?php while( have_rows("images") ): the_row(); ?>
            <?php if( get_sub_field("image") ): ?>
                <div data-wow-delay="<?php print get_row_index() * 0.25; ?>s" class="wow fadeIn image" style="background-image: url('<?php print get_sub_field("image"); ?>');"></div>
            <?php endif; ?>
        <?php endwhile; ?>
    </div>
</section><!-- block.block--<?php print get_row_layout(); ?>-->