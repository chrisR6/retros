<section class="block block--<?php print get_row_layout(); ?>"  style="background-image: url('<?php print (get_sub_field("image") ? get_sub_field("image") : ""); ?>');">
    <div class="container">
        <div class="clover rotating clover--large"></div> 
    </div>
</section><!-- block.block--<?php print get_row_layout(); ?>-->