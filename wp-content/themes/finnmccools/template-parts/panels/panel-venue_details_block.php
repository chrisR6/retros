
<section class="block block--<?php print get_row_layout(); ?>">
    <?php if( have_rows("venues") ): ?>
        <?php while( have_rows("venues") ): the_row(); ?>
            <?php if( get_sub_field("venue") ): $venue = get_sub_field("venue"); $title = get_sub_field("title_override"); $showAddress = get_sub_field("show_address"); ?>
                <div data-wow-delay="<?php print get_row_index() * 0.5; ?>s" class="wow fadeIn venue <?php print !$showAddress ? "details--hidden" : ""; ?>" 
                style="background-image:url('<?php print get_field("featured_image", $venue) ? get_field("featured_image", $venue) : ""; ?>');">
                    <div class="content">
                        <h4 class="title"><?php print (isset($title) && !empty($title) ? $title : get_the_title($venue)); ?></h4>
                        <?php if( (get_field("address", $venue) || get_field("location", $venue)) && $showAddress ): ?>
                            <div class="address">
                                <?php if( get_field("address", $venue) ): ?>
                                    <?php print get_field("address", $venue); ?>
                                <?php elseif( get_field("location", $venue) ): ?>
                                    <?php print get_field("location", $venue)["address"]; ?>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <?php if( get_field("phone_number", $venue) && $showAddress ): ?>
                            <div class="contact">
                                <?php if( get_field("phone_number", $venue) ): ?>
                                    Phone: <?php print finnmccools_generate_tel_link(get_field("phone_number", $venue)); ?>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <?php if( have_rows("buttons") ): ?>
                            <div class="buttons">
                                <?php while( have_rows("buttons") ): the_row(); ?>
                                    <a class="button" href="<?php print esc_attr(get_sub_field("button")["url"]); ?>" target="<?php print (get_sub_field("button")['target'] ? get_sub_field("button")['target'] : '_self'); ?>">
                                        <?php print esc_attr(get_sub_field("button")["title"]); ?>
                                    </a>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>
</section><!-- block.block--<?php print get_row_layout(); ?>-->