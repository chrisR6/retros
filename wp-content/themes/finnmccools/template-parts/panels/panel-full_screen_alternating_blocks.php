<section class="block block--<?php print get_row_layout(); ?>">
    <?php if( get_sub_field("block") ): ?>
        <?php while( have_rows("block") ): the_row(); ?>
            <div class="block__global__wrapper <?php print get_row_index() % 2 > 0 ? "right" : ""; ?>" style="background-image: url(<?php print get_sub_field("image"); ?>);">
                <div class="container">
                    <div class="row">
                        <div class="col-12 d-block d-xl-none">
                            <img src="<?php print get_sub_field("image"); ?>" class="mb-image" />
                        </div>
                        <div class="center <?php print get_row_index() % 2 > 0 ? "" : "offset-xl-7"; ?> <?php print get_sub_field("inclusions") ? "col-xl-6" : "col-xl-5"; ?>">
                            <div class="block__wrapper">
                                <?php if( get_sub_field("title") ): ?>
                                    <h5 class="block__title">
                                        <?php print get_sub_field("title"); ?>
                                    </h5>
                                <?php endif; ?>
                                <?php if( get_sub_field("content") ): ?>
                                    <div class="block__content">
                                        <?php print get_sub_field("content"); ?>
                                    </div>
                                    <?php if( get_sub_field("call_to_action_button") ): ?>
                                        <div class="cta_button">
                                            <a class="button" href="<?php print get_sub_field("call_to_action_button")["url"]; ?>" target="<?php print (get_sub_field("call_to_action_button")['target'] ? get_sub_field("call_to_action_button")['target'] : '_self'); ?>">
                                                <?php print get_sub_field("call_to_action_button")["title"]; ?>
                                            </a>
                                        </div>
                                    <?php endif; ?>
                                <?php elseif( get_sub_field("inclusions") ): ?>
                                    <div class="inclusions_wrapper">
                                        <?php while( have_rows("inclusions") ): the_row(); ?>
                                            <div class="inclusion">
                                                <div class="inclusion_title"><?php the_sub_field("title"); ?></div>
                                                <div class="inclusion_content"><?php the_sub_field("content"); ?></div>
                                            </div>
                                        <?php endwhile; ?>
                                        <div class="cta_button">
                                            <?php if( get_sub_field("call_to_action_button") ): ?>
                                                <a class="button" href="<?php print get_sub_field("call_to_action_button")["url"]; ?>" target="<?php print (get_sub_field("call_to_action_button")['target'] ? get_sub_field("call_to_action_button")['target'] : '_self'); ?>">
                                                    <?php print get_sub_field("call_to_action_button")["title"]; ?>
                                                </a>
                                            <?php endif; ?>
                                            <?php if( get_sub_field("additional_call_to_action_button") ): ?>
                                                    <a class="button" href="<?php print get_sub_field("additional_call_to_action_button")["url"]; ?>" target="<?php print (get_sub_field("additional_call_to_action_button")['target'] ? get_sub_field("call_to_action_button")['target'] : '_self'); ?>">
                                                        <?php print get_sub_field("additional_call_to_action_button")["title"]; ?>
                                                    </a>
                                            <?php endif; ?>
                                        
                                        </div>
                                        
                                    </div> 
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
</section><!-- block.block--<?php print get_row_layout(); ?>-->