<section class="block block--<?php print get_row_layout(); ?>"
style="background-image:url('<?php print get_sub_field("top_background_image") ? get_sub_field("top_background_image") : ""; ?>'), url(<?php print get_sub_field("bottom_background_image") ? get_sub_field("bottom_background_image") : ""; ?>);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="header">
                    <h3 class="title wow fadeIn"><?php print get_sub_field("title"); ?></h3>
                    <div class="subheading wow fadeIn" data-wow-delay="0.25s"><?php print get_sub_field("subheading"); ?></div>
                    <div class="description wow fadeIn" data-wow-delay="0.5s"><?php print get_sub_field("description"); ?></div>
                </div>
            </div>
        </div>
        <div class="row menu__wrapper">
            <div class="col-lg-4 left__column wow fadeIn" data-wow-delay="0.75s">
                <?php while( have_rows("sections") ): the_row(); ?>
                    <?php get_template_part( 'template-parts/content-single-menu-item' ); ?>
                <?php endwhile; ?>
            </div>

            <div class="col-lg-4 middle__column wow fadeIn" data-wow-delay="1s">

                <?php while( have_rows("sections_middle") ): the_row(); ?>
                    <?php get_template_part( 'template-parts/content-single-menu-item' ); ?>
                <?php endwhile; ?>
            </div>

            <div class="col-lg-4 right__column wow fadeIn" data-wow-delay="1.25s">

                <?php while( have_rows("sections_right") ): the_row(); ?>
                    <?php get_template_part( 'template-parts/content-single-menu-item' ); ?>
                <?php endwhile; ?>
            </div>
        </div>
            <div class="row text-center drinks_menu wow fadeIn">
                <div class="col-lg-12">
                    <?php if( get_sub_field("link_to_drinks_menu") ): ?>
                        <a href="<?php print get_sub_field("link_to_drinks_menu")['url']; ?>">see our drinks menu</a>
                    <?php endif; ?>
                </div>
            </div>
    </div>
</section><!-- block.block--<?php print get_row_layout(); ?>-->