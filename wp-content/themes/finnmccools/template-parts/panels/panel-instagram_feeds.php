<section class="block block--<?php print get_row_layout(); ?>">

    <!-- <div class="clover rotating rotating--med clover--alt clover--med"></div> -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4>Follow us</h4>
            </div>
        </div>
    </div>
    <div id="surfers"><?php print do_shortcode('[instagram-feed user="retros_surfersparadise"]'); ?></div>
    <div id="brisbane"><?php print do_shortcode('[instagram-feed]'); ?></div>
</section><!-- block.block--<?php print get_row_layout(); ?>-->
