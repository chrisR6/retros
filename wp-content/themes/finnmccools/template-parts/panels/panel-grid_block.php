<section class="block block--<?php print get_row_layout(); ?>">
    <?php if( get_sub_field("grid_items") ): ?>
        <?php if( have_rows("grid_items") ): ?>
            <div class="container">
                <div class="clover rotating clover--large"></div>
                <!-- <div class="clover rotating rotating--med clover--alt clover--med"></div> -->
                <div class="row">
                    <?php while( have_rows("grid_items") ): the_row(); ?>
                        <?php if( get_sub_field("split_block") ): ?>
                            <div data-wow-delay="<?php print get_row_index() * 0.05; ?>s" class="wow fadeIn grid__item split col-xl-3 col-lg-3 col-sm-6 col-12">
                                <?php while( have_rows("splits") ): the_row(); ?>
                                    <a href="<?php print get_sub_field("link")["url"]; ?>" target="<?php print (get_sub_field("link")['target'] ? get_sub_field("link")['target'] : '_self'); ?>">
                                        <div class="item__inner" style="background-image: url('<?php print (get_sub_field("image") ? get_sub_field("image") : ""); ?>');">
                                            <div class="item__content">
                                                <?php print get_sub_field("link")["title"]; ?>
                                            </div>
                                        </div>
                                    </a>
                                <?php endwhile; ?>
                                <div class="item__inner">
                                    <div class="item__content">
                                        <?php print get_sub_field("block_title"); ?>
                                    </div>
                                </div>
                            </div>
                        <?php else: ?>
                            <?php if( get_sub_field("link") ): ?>
                                <div data-wow-delay="<?php print get_row_index() * 0.05; ?>s" class="wow fadeIn grid__item col-xl-3 col-lg-3 col-sm-6 col-12">
                                    <a href="<?php print get_sub_field("link")["url"]; ?>" target="<?php print (get_sub_field("link")['target'] ? get_sub_field("link")['target'] : '_self'); ?>">
                                        <div class="item__inner" style="background-image: url('<?php print (get_sub_field("image") ? get_sub_field("image") : ""); ?>');">
                                            <?php if( get_sub_field("link")["title"] ): ?>
                                                <div class="item__content">
                                                    <?php print get_sub_field("link")["title"]; ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </a>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </div>
                <div class="clover rotating rotating--med clover--large bottom"></div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
</section><!-- block.block--<?php print get_row_layout(); ?>-->