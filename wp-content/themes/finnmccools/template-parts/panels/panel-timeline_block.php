<section id="history" class="block block--<?php print get_row_layout(); ?>">
    <div class="container">
        <div class="line"></div>
        <?php while( have_rows("history") ): the_row(); 
            $year = get_sub_field("year");
            $title = get_sub_field("title");
            $content = get_sub_field("content");
            $isRightAligned = get_row_index() % 2 == 0;
        ?>
            <div data-wow-delay="<?php print get_row_index() * 0.05; ?>s" class="row wow fadeIn <?php print $isRightAligned ? "odd" : ""; ?>">
                <div class="col-lg-6 <?php print ($isRightAligned ? "offset-lg-6": ""); ?>">
                    <div class="timeline__entry">
                        <div class="year"><?php print $year; ?></div>
                        <div class="title"><?php print $title; ?></div>
                        <div class="content"><?php print $content; ?></div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
        <div class="clover rotating rotating--med clover--small"></div>
    </div>
</section><!-- block.block--<?php print get_row_layout(); ?>-->