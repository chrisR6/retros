<?php
    $hasRightColumn = get_sub_field("image");
    $indent = get_sub_field("indent_content");
    $contentClass = "col-lg-6";
    if( $hasRightColumn ) {
        if( $indent ) {
            $contentClass = "col-lg-5 offset-lg-1";
        }
    } else {
        if( $indent ) {
            $contentClass = "col-lg-8 offset-lg-1";
        } else {
            $contentClass = "col-lg-9";
        }
    }
?>
<section class="block block--<?php print get_row_layout(); ?>">
    <div class="container">
        <div class="row">
            <div class="<?php print $contentClass; ?> content text-center text-lg-left">
                <?php if( get_sub_field("title") ): ?>
                    <h5 class="title wow fadeIn" data-wow-delay="0.25s"><?php print get_sub_field("title"); ?></h5>
                <?php endif; ?>
                <?php if( get_sub_field("content") ): ?>
                    <div class="body_content wow fadeIn" data-wow-delay="0.5s"><?php print get_sub_field("content"); ?></div>
                <?php endif; ?>
                <?php if( get_sub_field("include_button") && get_sub_field("button") ): ?>
                    <div class="cta_button wow fadeIn" data-wow-delay="0.75s">
                        <a class="button" href="<?php print get_sub_field("button")["url"]; ?>" target="<?php print (get_sub_field("button")['target'] ? get_sub_field("button")['target'] : '_self'); ?>">
                            <?php print get_sub_field("button")["title"]; ?>
                        </a>
                    </div>
                <?php endif; ?>
                
            </div>
            <?php if( $hasRightColumn ): ?>
                <div class="col-lg-5 offset-lg-1 image wow fadeIn" data-wow-delay="1s" style="background-image: url('<?php print get_sub_field("image"); ?>');">
                    <?php if( get_sub_field("floating_box") && get_sub_field("floating_box_content") ): ?>
                        <div class="block block--header">
                            <?php if( get_sub_field("floating_box_content") ): ?>
                                <div class="box_content"><?php print get_sub_field("floating_box_content"); ?></div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            
        </div>
    </div>
</section><!-- block.block--<?php print get_row_layout(); ?>-->