<section class="block block--<?php print get_row_layout(); ?>">
<?php if( get_sub_field("venue") ): 
    date_default_timezone_set('Australia/Brisbane');
    $date_now = current_time('Y-m-d H:i:s');
    $now = getStartTimeframe( -1 );
    $end = getEndTimeframe( -1 );
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1; 
    $queryArgs = array(
        'paged'           => $paged, 
        'post_type'  => 'events',
        'order'      => 'asc',
        'orderby'    => 'meta_value',
        'meta_key'   => 'event_start_date',
        'meta_query' => array(
            'relation'    => 'AND',
            array(
                'relation'    => 'AND',
                array(
                    'key'     => 'event_start_date',
                    'value'   => array($now, $end),
                    'compare' => 'BETWEEN',
                    'type'    => 'DATE'
                ),
                array(
                    'key'     => 'venue',
                    'value'   => get_sub_field("venue"),
                    'compare' => '=',
                )
            )
        )
    );
    $events = new WP_Query($queryArgs);

    ?>
    
        <?php 
        if( $events->have_posts() ) { ?>
            <div class="container">
                <div class="clover rotating clover--large"></div> 
            </div>
            <?php
            $ii = 0; while( $events->have_posts() ): 
                $events->the_post();
                ?>
                <div class="event__container <?php print $ii % 2 == 0 ? "odd" : ""; ?>">
                    <div class="container">
                        <div data-wow-delay="<?php print $ii * 0.25; ?>s" class="wow fadeIn row event__wrapper <?php print $ii % 2 == 0 ? "odd" : ""; ?>">
                            <div class="wrapper col-lg-12">
                                <div class="event__details <?php print $ii % 2 == 0 ? "odd" : ""; ?>">
                                    <div class=" col-lg-2">

                                        <?php if( get_field("teaser_image") ): ?>
                                            <img class="d-block d-lg-none mobile-image" src="<?php print get_field("teaser_image"); ?>" />
                                        <?php elseif( get_field("event_image") ): ?>
                                            <img class="d-block d-lg-none mobile-image" src="<?php print get_field("event_image"); ?>" />
                                        <?php else: ?>
                                        <?php endif; ?>
                                        <div class="block block--header">
                                            <div class="content">
                                            <?php
                                            $date = get_field('event_start_date');
                                            // $theDate = date("l jS F", strtotime($date));
                                            // $theStartTime = date("g:ia", strtotime($date));
                                            $theDate = date("j ", strtotime($date));
                                            $theMonth = date("M ", strtotime($date));
                                            $theStartTime = date("g:ia", strtotime($date));
                                            // $theEndTime = date("g:ia", strtotime(get_field('end_time', get_queried_object_id())));
                                            ?>
                                            <div class="block date"><?php print $theDate; ?></div>
                                            <div class="block month"><?php print $theMonth; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 event__content">
                                        <div class="col-lg-4 event__image">
                                            <?php if( get_field("teaser_image") ): ?>
                                                <img class="d-none d-lg-block" src="<?php print get_field("teaser_image"); ?>" />
                                            <?php elseif( get_field("event_image") ): ?>
                                                <img class="d-none d-lg-block" src="<?php print get_field("event_image"); ?>" />
                                            <?php else: ?>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-lg-6 event__content__container">
                                            <h4><?php the_title(); ?></h4>
                                            <span class="date"><?php print date("l ga", strtotime('+10 hours',strtotime($date))); ?></span>
                                            <!-- <span class="venue">Retros <?php print get_field("venue")->post_title; ?></span> -->
                                            <div class="content"><?php print wp_trim_words(get_the_content(), 20, false); ?></div>
                                        </div>
                                        <div class="col-lg-2 event__image">
                                            <div class="cta_button">
                                                <a class="button" href="<?php print get_the_permalink(); ?>">
                                                    INFO
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
           $ii++; endwhile; 
        ?>

        <?php if( $events->max_num_pages > 1 ) : ?>
            <div class="row pagination <?php print ($events->max_num_pages > 1 ? "" : "d-none d-lg-flex"); ?>">
                <div class="col-lg-12 page-numbers text-center">
                    <?PHP
                    previous_posts_link( 'PREV',$events->max_num_pages ); 
                    next_posts_link( 'NEXT',$events->max_num_pages );
                    ?>
                </div>
            </div>
        <?php endif; ?>
            <div class="container">
                <div class="clover rotating clover--large end"></div> 
            </div>
        <?php } else {  ?>
        <div class="container">
            <div class="row noresults">
                <div class="col-lg-12 text-center">
                    <h4>No upcoming events</h4>
                </div>
            </div>
        </div>
<?php
        } ?>
    <?php endif; ?>
</section><!-- block.block--<?php print get_row_layout(); ?>-->