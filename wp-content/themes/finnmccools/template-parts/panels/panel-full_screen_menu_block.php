<section class="block block--<?php print get_row_layout(); ?>">
    <div class="container">
        <div class="row">
            <?php while( have_rows("menu_blocks") ): the_row(); ?>
                <?php $tag = "div"; $href = ""; if( get_sub_field("button") ){ $tag = "a"; $href = "href='".get_sub_field("button")["url"]."'"; } ?>
                <<?php print $tag; ?> <?php print $href; ?>  class="<?php print get_row_index() == 1 ? "first" : ""; ?> icon icon--<?php print get_sub_field("icon"); ?> col-lg-6">
                    <div class="menu_block">
                        <div class="title"><?php the_sub_field("title"); ?></div>
                    </div>
                </<?php print $tag; ?>>
            <?php endwhile; ?>
        </div>
    </div>
    <!-- <div class="menu_wrapper">
        <?php while( have_rows("menu_blocks") ): the_row(); ?>
            <div class="menu_block">
                <div class="title"><?php the_sub_field("title"); ?></div>
                <?php if( get_sub_field("button") ): ?>
                    <div class="cta_button">
                        <a class="button" href="<?php print get_sub_field("button")["url"]; ?>">
                            <?php print get_sub_field("button")["title"]; ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        <?php endwhile; ?>
    </div> -->
</section><!-- block.block--<?php print get_row_layout(); ?>-->