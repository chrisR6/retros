<section class="block block--<?php print get_row_layout(); ?>"
    style="background-image:url('<?php print get_sub_field("background_image") ? get_sub_field("background_image") : ""; ?>');">
    <div class="container">
        <div class="row">
            <?php if( get_sub_field("block_title") || get_sub_field("block_image") || get_sub_field("block_content") ): ?>
                <div class="wow fadeIn block block--header">
                    <?php if( get_sub_field("block_title") ): ?>
                        <div class="title"><?php print get_sub_field("block_title"); ?></div>
                    <?php endif; ?>
                    <?php if( get_sub_field("block_image") ): ?>
                        <div class="title"><img src="<?php print get_sub_field("block_image"); ?>" /></div>
                    <?php endif; ?>
                    <?php if( get_sub_field("block_content") ): ?>
                        <div class="content"><?php print get_sub_field("block_content"); ?></div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <?php while( have_rows("block") ): the_row(); ?>
                <div data-wow-delay="<?php print get_row_index() * 0.5; ?>s" class="wow fadeIn grid__item offset-lg-2 col-lg-10 style--<?php print get_sub_field("style"); ?> <?php print get_row_index() % 2 == 0 ? "alt" : ""; ?>">
                    <div class="item__inner">
                        <div class="image"  style="background-image: url('<?php print (get_sub_field("image") ? get_sub_field("image") : ""); ?>');">
                        </div>
                        <div class="content">
                            <?php if( get_sub_field("title") ): ?>
                                <div class="item__title">
                                    <h5><?php print get_sub_field("title"); ?></h5>
                                </div>
                            <?php endif; ?>
                            <?php if( get_sub_field("content") ): ?>
                                <div class="item__content">
                                    <?php print get_sub_field("content"); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if( get_row_index()-1 == count(get_sub_field("block")) ): ?>
                        <div class="clover rotating clover--large"></div>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</section><!-- block.block--<?php print get_row_layout(); ?>-->