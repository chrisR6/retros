<section class="block block--<?php print get_row_layout(); ?> <?php print get_sub_field("push_block_up") ? "push--up": ""; ?>"
    >
    <div class="container">
        <div class="clover rotating clover--large"></div> 
        <div class="clover rotating rotating--med clover--alt clover--med"></div>
        <div class="row">
            <div class="col-lg-12">
                <div class="item__inner">
                    <div data-wow-delay="0.5s" class="content wow fadeIn <?php print get_sub_field("vertically_centre_the_text_with_the_image") ? "align--center" : ""; ?>">
                        <?php if( get_sub_field("title") ): ?>
                            <div class="item__title">
                                <div class="h4"><?php print get_sub_field("title"); ?></div>
                            </div>
                        <?php endif; ?>
                        <?php if( get_sub_field("top_content") ): ?>
                            <div class="item__content">
                                <?php print get_sub_field("top_content"); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php if( get_sub_field("image") ): ?>
                        <div data-wow-delay="0.75s" class="image wow fadeIn <?php print get_sub_field("vertically_centre_the_text_with_the_image") ? "" : "height-adjusted"; ?>"  style="background-image: url('<?php print (get_sub_field("image") ? get_sub_field("image") : ""); ?>');">
                            <img src="<?php print get_sub_field("image"); ?>" class="d-block d-xl-none mb-image" />
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="clover rotating rotating--med clover--small"></div>
    </div>
    <?php if( get_sub_field("bottom_content") || get_sub_field("bottom_image") ): ?>
        <div class="bottom__wrapper" style="background-image:url('<?php print get_sub_field("background_image") ? get_sub_field("background_image") : ""; ?>');">
            <div class="container">
                <div data-wow-delay="1s" class="wow fadeIn row bottom">
                    <div class="col-lg-12">
                        <div class="item__inner">
                            <div class="content ">
                                <?php if( get_sub_field("bottom_content") ): ?>
                                    <div class="item__content">
                                        <?php print get_sub_field("bottom_content"); ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="image "  style="background-image: url('<?php print (get_sub_field("bottom_image") ? get_sub_field("bottom_image") : ""); ?>');">
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php if( get_sub_field("block_content") ): ?>
                    <div data-wow-delay="1.15s" class="wow fadeIn block block--header">
                        <?php if( get_sub_field("block_content") ): ?>
                            <div class="content"><?php print get_sub_field("block_content"); ?></div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>
</section><!-- block.block--<?php print get_row_layout(); ?>-->