<section class="block block--<?php print get_row_layout(); ?>">
<?php if( get_sub_field("image") ): ?>
    <div class="container">
        <div class="row">
            <div class="wow fadeIn col-lg-12">
                <img src="<?php print get_sub_field("image"); ?>" />
            </div> 
        </div>
    </div>
    <?php endif; ?>
</section><!-- block.block--<?php print get_row_layout(); ?>-->