<section class="block block--<?php print get_row_layout(); ?>"
    style="background-image: url('<?php print (get_sub_field("background_image") ? get_sub_field("background_image") : ""); ?>');">
    <div class="container">
        <?php if( get_sub_field("image") ): ?>
            <div class="row">
                <div class="col-lg-12">
                    <img src="<?php print get_sub_field("image"); ?>" />
                </div>
            </div>
        <?php endif; ?>
        <?php if( get_sub_field("floating_box") && get_sub_field("floating_box_content") ): ?>
            <div data-wow-delay="0.5s" class="wow fadeIn block block--header">
                <?php if( get_sub_field("floating_box_content") ): ?>
                    <div class="content"><?php print get_sub_field("floating_box_content"); ?></div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</section><!-- block.block--<?php print get_row_layout(); ?>-->