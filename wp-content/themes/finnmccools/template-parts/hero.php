<?php
    $pageId = get_the_ID();
    if( is_404() ) {
        $pageId = get_option( 'page_on_front' );
    }
    $includeLargeGapBelow = get_field("large_gap_below_header", $pageId);
    if( is_single() && "events" == get_post_type() ) {
        get_template_part("template-parts/hero-events");
        return;
    }
?>
<?php if( get_field("slides") ): ?>
    <div class="hero_container container">
        <section class="hero <?php print ($includeLargeGapBelow ? "push--down" : ""); ?>">
            <div id="hero-slider" class="carousel slide" data-ride="carousel" data-hover="false" data-interval="8000">
                <div class="overlay"></div>
                <div class="carousel-inner">
                    <?php while( have_rows("slides") ): the_row(); 
                        $headerType = get_sub_field("header_type");
                        $text = get_sub_field("header_content");

                        $image = get_sub_field("image");
                        $video = get_sub_field("video");
                        $thumbnail = get_sub_field("thumbnail");
                        $imageToUse = "";

                        $isContentRightAligned = get_sub_field("align_text_to_the_right");


                        if( is_404() ) {
                            $includeLargeGapBelow = false;
                        }

                        if( $headerType == "image" ) {
                            $imageToUse = $image;
                        } else if( $headerType == "video" ) {
                            $imageToUse = $thumbnail;
                        }

                        if( is_single() && get_post_type() == "events" ) {
                            $imageToUse = get_sub_field("event_image");
                        }
                    ?>
                    <div class="carousel-item <?php print ($isContentRightAligned ? "content--right" : ""); ?> <?php print get_row_index() == 1 ? "active": ""; ?>" style="background-image: url(<?php print ($imageToUse ? $imageToUse : ""); ?>);">
                        <?php if( $headerType == "video" && $video ): ?>
                            <div class="video row">
                                <?php if( $video['mime_type'] == 'video/quicktime') : ?>
                                    <video autoplay muted loop id="myVideo" src="<?php print $video['url']; ?>"></video>
                                <?php else: ?>
                                    <video autoplay muted loop id="myVideo">
                                        <source src="<?php print $video['url']; ?>" type="<?php print $video['mime_type']; ?>">
                                    </video>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <div class="row">
                            <?php if( $text ): ?>
                                <div class="wow fadeIn <?php print ($isContentRightAligned ? "offset-lg-6" : "") ; ?> col-lg-8 col-xl-7 content">
                                    <?php print $text; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php if( count(get_field("slides")) > 1 ): ?>
                    <ol class="carousel-indicators">
                        <?php for($ii = 0; $ii < count(get_field("slides")); $ii++): ?>
                            <li data-target="#hero-slider" data-slide-to="<?php print $ii; ?>" class="<?php print $ii == 0 ? "active": ""; ?>"></li>
                        <?php endfor; ?>
                    </ol>
                <?php endif; ?>
            </div>
        </section>
    </div>
<?php endif; ?>
