<?php
    $pageId = get_the_ID();
    $imageToUse = get_field("event_image", $pageId);
?>
<section class="hero">
    <div class="container" style="background-image: url(<?php print ($imageToUse ? $imageToUse : ""); ?>);">
        <div class="row">
            <?php if( $text ): ?>
                <div class="wow fadeIn <?php print ($isContentRightAligned ? "offset-lg-6" : "") ; ?> col-lg-6 content">
                    <?php print $text; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
