<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package finnmccools
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function finnmccools_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'finnmccools_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function finnmccools_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'finnmccools_pingback_header' );


function my_acf_init() {
    acf_update_setting('google_api_key', 'AIzaSyAzjRz_i2OntIYrj-jZxzg-CqqE0NfUtzM');
}
add_action('acf/init', 'my_acf_init');


function finnmccools_parse_dynamic_tags( $content ) {
	$tags = ["{date}", "{copy}"];
	$replace = [date('Y'), "&copy;"];
	$content = str_replace($tags, $replace, $content);
	print $content;
}

function finnmccools_generate_tel_link( $phone ) {
	$sanitized_phone = str_replace(" ", "", $phone);
	$sanitized_phone = preg_replace('/\D/', '', $phone);
	return "<a href='tel:".$sanitized_phone."'>".$phone."</a>";
}

function finnmccools_social_icons() {
	ob_start();
	get_template_part( 'template-parts/social' );

	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}

add_filter( 'wpcf7_validate', 'email_already_in_db', 10, 2 );

function email_already_in_db ( $result, $tags ) {
    // retrieve the posted email
    $form  = WPCF7_Submission::get_instance();
    $postcode = $form->get_posted_data('postcode');
	// if( !is_numeric($postcode) ) {
	// 	$result->invalidate('postcode', 'Please enter a valid postcode');
	// }

    $dobmonth = $form->get_posted_data('dobmonth');

	if( !is_numeric($dobmonth) ) {
		$result->invalidate('dobmonth', 'Please enter a valid month');
	}

    $dobyear = $form->get_posted_data('dobyear');
	if( !is_numeric($dobyear) ) {
		$result->invalidate('dobyear', 'Please enter a valid year');
	}

    return $result;
}


add_action( 'init', 'post_type_events_init' );

function post_type_events_init() {
	register_post_type( 'events', array(
		'labels' => array(
			'name'               => 'Events',
			'singular_name'      => 'Event',
			'menu_name'          => 'Events',
			'name_admin_bar'     => 'Events',
			'all_items'          => 'All Events',
			'add_new'            => 'Add New',
			'add_new_item'       => 'Add New Event',
			'edit_item'          => 'Edit Event',
			'new_item'           => 'New Event',
			'view_item'          => 'View Event',
			'search_items'       => 'Search Events',
			'not_found'          => 'No Events Found',
			'not_found_in_trash' => 'No Events Found in Trash',
			'parent_item_colon'  => 'Parent Event'
		),
		'public' => true,
		'menu_icon' =>'dashicons-awards',
		'capability_type' => 'page',
		'supports' => array(
            'title',
			'editor'
		),
		'has_archive' => false
	));
	register_taxonomy(  
		'category',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
		'events',        //post type name
		array(  
			'hierarchical' => true,  
			'labels' => array(
				'name' => 'Category',
				'menu_name' => 'Category',
				'new_item_name' => __( 'New Category' ),
				'add_new_item' => __( 'Add New Category' )
			),
			// 'label' => 'Stage (Front Page)',  //Display name
			'query_var' => true,
			'rewrite' => array(
				'slug' => 'category', // This controls the base slug that will display before each term
				'with_front' => false // Don't display the category base before 
			)
		)  
	);
}

function getStartTimeframe( $timeframe ) {
    if( $timeframe == 0 ) {
        return date('Y-m-d H:i:s', strtotime('00:00:00'));
        // Start time is today at midnight
    } else if( $timeframe == 1 ) { //tomorrow
        return date('Y-m-d H:i:s', strtotime('tomorrow midnight'));
    } else if( $timeframe == 7 ) { //this week
        return date('Y-m-d H:i:s', strtotime('00:00:00'));
    } else if( $timeframe == 30 ) { //upcoming
        return date('Y-m-d H:i:s', strtotime('00:00:00'));
    } else if( $timeframe == -1 ) { // all
        return date('Y-m-d H:i:s', strtotime('00:00:00'));
    } else {
        return null;
    }
}
function getEndTimeframe( $timeframe ) {
    if( $timeframe == 0 ) {
        return date('Y-m-d H:i:s', strtotime('23:59:59'));
    } else if( $timeframe == 1 ) {
        return date('Y-m-d H:i:s', strtotime('tomorrow'));
    } else if( $timeframe == 7 ) {
        return date('Y-m-d H:i:s', strtotime('+7 days'));
    } else if( $timeframe == 30 ) {
        return date('Y-m-d H:i:s', strtotime('+30 days'));
    } else if( $timeframe == -1 ) {
        return date('Y-m-d H:i:s', strtotime('+10 years'));
    } else {
        return null;
    }
}
// add_action( 'wpcf7_mail_sent', 'after_submission', 10, 1 );
function after_submission( $contact_form ) {
	return;
	die();
	$submission = WPCF7_Submission::get_instance();

	//brisbane = mf5811748b434ca
	// goldcoast = mfab07d2da8d5
    if ( $submission ) {
        $data = $submission->get_posted_data();
		$name =  $data['first-name'];
		$lastname =  $data['last-name'];
		$email =  $data['email'];
		// $postcode =  $data['postcode'];
		$location = $data['menu-495'];
		$dobmonth =  $data['dobmonth'];
		$dobyear =  $data['dobyear'];
    	$endpoint_url = 'https://myguestlist.com/mgl/formreceiver.php';
		$formId = "";
		$body = array(
			"formID" => "mf5811748b434ca",
			"PatronName" => $name,
			"PatronSurname" => $lastname,
			"PatronEmail" => $email,
			"PatronPostcode" => $postcode,
			"DOB_dd" => "1",
			"DOB_mm" => $dobmonth,
			"DOB_yyyy" => $dobyear,
		);
		if( $location == "FORTITUDE VALLEY" ) {
			// $formId = "mf5811748b434ca";
			$body['formID'] = "mf5811748b434ca";
		} else if( $location == "SURFERS PARADISE") {
			// $formId = "mfab07d2da8d5";
			$body['formID'] = "mfab07d2da8d5";
		}
		if( $location == "FORTITUDE VALLEY" || $location == "SURFERS PARADISE" ) {
			$response = wp_remote_post( $endpoint_url, array( 'body' => $body ) );
		} else {
			// subscribe to bris
			$body['formID'] = "mf5811748b434ca";
			$response = wp_remote_post( $endpoint_url, array( 'body' => $body ) );

			// subscribe to gc
			$body['formID'] = "mfab07d2da8d5";
			$response = wp_remote_post( $endpoint_url, array( 'body' => $body ) );
		}
		
    }
}