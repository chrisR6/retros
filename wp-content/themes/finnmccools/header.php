<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package finnmccools
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="icon" href="<?php print get_template_directory_uri(); ?>/dist/img/favicon.png" type="image/x-icon"/>
	<link rel="shortcut icon" href="<?php print get_template_directory_uri(); ?>/dist/img/favicon.png" type="image/x-icon"/>

	<?php wp_head(); ?>
	<!-- <script src="<?php print get_template_directory_uri(); ?>/dist/js/displayform.js" type="text/javascript"></script> -->
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TG5SJZ3');</script>
	<!-- End Google Tag Manager -->

</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TG5SJZ3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php wp_body_open(); ?>
<div id="page" class="site">
	<?php if( get_field("show_banner", "options") ): ?>
		<div class="banner">
			<div class="message">
				<?PHP
				date_default_timezone_set('Australia/Brisbane');
				$date_now = current_time('Y-m-d H:i:s');
				$now = getStartTimeframe( -1 );
				$end = getEndTimeframe( -1 );
				$queryArgs = array(
					'paged'           => $paged, 
					'post_type'  => 'events',
					'order'      => 'asc',
					'orderby'    => 'meta_value',
					'meta_key'   => 'event_start_date',
					'posts_per_page' => 1,
					'meta_query' => array(
						'relation'    => 'AND',
						array(
							'relation'    => 'AND',
							array(
								'key'     => 'event_start_date',
								'value'   => array($now, $end),
								'compare' => 'BETWEEN',
								'type'    => 'DATE'
							)
						)
					)
				);
				$events = new WP_Query($queryArgs);
				if( $events->have_posts() ):
					while( $events->have_posts() ): $events->the_post();
						// print_r('here');
						$date = get_field('event_start_date');
						$theDate = date("j ", strtotime($date));
						$theMonth = date("M ", strtotime($date));
						$venue = get_field("venue");
						$link = get_the_permalink();
						print get_the_title() . ': ' . $theDate . ' ' . $theMonth . ( $venue ? ' in ' . $venue->post_title : "") . " click <a href='{$link}'>here</a> for details" ;
						//KRISTIE LEA LIVE THIS FRIDAY IN SURFERS PARADISE CLICK HERE FOR DETAILS
					endwhile;
				endif;
				wp_reset_postdata();
				?>
				<?php //print get_field("banner_content", "options"); ?>
			</div>
		</div>
	<?php endif; ?>
	<header class="main-nav">
		<nav class="navbar navbar-expand-lg">
			<!-- <a class="d-none d-lg-block navbar-brand" href="<?php print home_url(); ?>">
				<img src="<?php print get_template_directory_uri(); ?>/dist/img/logo.png" alt="Retros Logo" title="Retros Logo" />
			</a> -->
			<div class="container">
				<div class="row">
					<div class="col-lg-12 d-lg-flex align-items-center">
						<a class="navbar-brand" href="<?php print home_url(); ?>">
							<img src="<?php print get_template_directory_uri(); ?>/dist/img/logo.png" alt="Retros Logo" title="Retros Logo" />
						</a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="fullScreenNav" aria-expanded="false" aria-label="Toggle navigation">
						<div class="pipe"></div>
							<div class="pipe"></div>
							<div class="pipe"></div>
							<div class="close" style="display: none;">x</div>
						</button>
						<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'menu-1',
								'depth'	          => 3, // 1 = no dropdowns, 2 = with dropdowns.
								'container'       => 'div',
								'container_id'	  => 'headerNav',
								'container_class' => 'collapse navbar-collapse justify-content-end',
								'menu_class'      => 'navbar-nav ',
								'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
								'walker'          => new WP_Bootstrap_Navwalker(),
							) );
						?>
						<?php get_template_part( 'template-parts/social' ); ?>
					</div>
				</div>
			</div>
		</nav>
	</header>
	<?php get_template_part( 'template-parts/hero' ); ?>

<div class="women"></div>