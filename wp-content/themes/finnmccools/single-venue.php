<?php
/**
 * The template for displaying event details
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package finnmccools
 */

get_header();
?>

	<main id="primary" class="site-main">
        <div class="block_container layout--content_block">
            <section class="block block--content_block <?php print get_field("opening_section_image") ? "" : "floatingbox"; ?>">
                <div class="container">
                    <div class="clover rotating clover--large"></div> 
                    <div class="clover rotating rotating--med clover--alt clover--small"></div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="item__inner">
                                <div class="content">
                                    <?php if( get_field("opening_content") ): ?>
                                        <div class="item__content">
                                            <?php print get_field("opening_content"); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <?php if( get_field("opening_section_image") ): ?>
                                    <div data-wow-delay="0.75s" class="image wow fadeIn"  style="background-image: url('<?php print get_field("opening_section_image"); ?>');">
                                    <img src="<?php print get_field("opening_section_image"); ?>" class="d-block d-xl-none mb-image" />
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="clover rotating rotating--med clover--med"></div>
                </div>
            </section><!-- block.block--content_block-->
        </div>
        <div class="block_container">
            <section class="block block--info" style="background-image:url('<?php print get_field("map_content_background_image") ? get_field("map_content_background_image") : ""; ?>');">
                <div class="container">
                    <div class="row">
                        <div class="wow fadeIn col-lg-7 section">
                            <div class="item__inner">
                                <div class="content">
                                    <?php if( get_field("content") ): ?>
                                        <div class="item__content">
                                            <?php print get_field("content"); ?>
                                        </div>
                                    <?php endif; $id = uniqid(); ?>
                                    <div class="map_wrap">
                                        <div class="overlay"></div>
                                        <div class="map" id="map-<?php print $id; ?>">
                                            <div class="acf-map">
                                                <?php if( get_field("location") ): $location = get_field("location"); ?>
                                                    <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                                                    <script>
                                                        setTimeout(function() {
                                                            if( window.maps.loaded.length > 0 ) {
                                                                window.maps.loaded.forEach( map => {
                                                                    if( map ) {
                                                                        if( map.markers ) {
                                                                            map.markers.forEach( marker => {
                                                                                marker.setMap(null);
                                                                            });
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            window.maps.loaded = [];
                                                            window.maps.new_map(jQuery("#map-<?php print $id; ?>"));
                                                        }, 500);
                                                    </script>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-wow-delay="0.5s" class="wow fadeIn col-lg-5 section">
                            <div class="item__inner">
                                <div class="content">
                                    <?php if( get_field("venue_quick_information") ): ?>
                                        <div class="venue_quick_information">
                                            <?php while( have_rows("venue_quick_information") ): the_row(); ?>
                                                <div class="quick_information style--<?php print get_sub_field("icon_style"); ?>">
                                                    <div class="inner"><?php print get_sub_field("content"); ?></div>
                                                </div>
                                            <?php endwhile; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- block.block--info-->

            <section class="block block--service">
                <div class="container">
                    <div class="row">
                        <?php while( have_rows("service") ): the_row(); ?>
                            <div data-wow-delay="<?php print get_row_index() * 0.25; ?>s" class="wow fadeIn service col-lg-4">
                                <div class="service__inner style--<?php print get_sub_field("service_style"); ?>">
                                    <h5 class="title"><?php print get_sub_field("title"); ?></h5>
                                    <div class="content"><?php print get_sub_field("content"); ?></div>
                                    <?php if( get_sub_field("button") ): ?>
                                        <div class="cta_button">
                                            <a class="button" target="<?php print (get_sub_field("button")["target"] ? get_sub_field("button")["target"] : "_self"); ?>" href="<?php print get_sub_field("button")["url"]; ?>">
                                                <?php print get_sub_field("button")["title"]; ?>
                                            </a>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </section><!-- block.block--service-->

            <section class="block block--media">
                <div class="container">
                    <div class="row">
                        <div data-wow-delay="1s" class="wow fadeIn col-lg-12">
                            <div class="pr_media">
                                <?php print get_field("pr_media_content"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- block.block--service-->

            <section class="block block--gallery">
                <?php
                $images = get_field('image_gallery');
                if( $images ): ?>
                    <?php $ii = 0;foreach( $images as $image ): ?>
                    <div class="image wow fadeIn" data-wow-delay="<?php print $ii * 0.25; ?>s" style="background-image: url(<?php print $image; ?>);"></div>
                    <?php $ii++; endforeach; ?>
                <?php endif; ?>
            </section><!-- block.block--service-->

            <div class="block_container layout--alternating_blocks">
                <section class="block block--alternating_blocks">
                    <div class="container">
                        <div class="row">
                            <?php if( get_field("events_floating_box") ): ?>
                                <div class="wow fadeIn block block--header">
                                    <?php if( get_field("events_floating_box") ): ?>
                                        <div class="content"><?php print get_field("events_floating_box"); ?></div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            <div class="grid__item offset-lg-1 col-lg-11 style--dark">
                                <div class="item__inner">
                                    <div data-wow-delay="0.25s" class="wow fadeIn image" style="background-image: url('<?php print (get_field("events_image") ? get_field("events_image") : ""); ?>');">
                                    </div>
                                    <div data-wow-delay="0.5s" class="wow fadeIn events_content content">
                                        <?php if( get_field("events_content") ): ?>
                                            <div class="item__content">
                                                <?php print get_field("events_content"); ?>
                                            </div>
                                            <div class="button_wrapper">
                                                <a href="<?php print get_permalink(get_page_by_title("What's on " . get_the_title())); ?>">
                                                    See What's On
                                                </a>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="book_button row">
                            <div class="wow fadeIn col-lg-12 text-center">
                                <a href="<?php /* 207 = brisbane */ print (get_the_ID() == 207 ? "https://www.ivvy.com.au/venue/order/start?v=ce6ca2f1894ebd23e03ad1bb094a17be" : "https://www.ivvy.com.au/venue/order/start?v=7f56f0dbd09464dee83dbb26b3ee71ef"); ?>" target="_blank" rel="noreferrer,noopener" class="button">BOOK</a>
                            </div>
                        </div>
                    </div>
                </section><!-- block.block--<?php print get_row_layout(); ?>-->
            </div>

            <!-- <div class="block_container layout--full_screen_menu_block">
                <section class="block block--full_screen_menu_block">
                    <div class="container">
                        <div class="row">
                            <div class="wow fadeIn first col-lg-6">
                                <div class="menu_block">
                                    <div class="title">Click to view What's On</div>
                                    <div class="cta_button">
                                        <a class="button" href="<?php print get_permalink(get_page_by_title("What's on")); ?>">
                                            WHAT'S ON
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div data-wow-delay="0.5s" class="wow fadeIn col-lg-6">
                                <div class="menu_block">
                                    <div class="title">Click to view our Functions page</div>
                                    <div class="cta_button">
                                        <a class="button" href="<?php print get_permalink("Functions"); ?>">
                                            FUNCTIONS
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div> -->
        </div>
	</main><!-- #main -->

<?php
get_footer();
