//=require map.js
jQuery(document).ready(function($) {
    new WOW().init();

    $("select").select2({
        minimumResultsForSearch: -1
    });

    $(".navbar-toggler").on("click", function(e) { 
        // $(".fullscreen-menu").css("bottom", "0");
        $("body").addClass("menuOpen");
        setTimeout(function() {
            $("body").addClass("menuOpened");
        }, 1);
    });

    $(".fullscreen-menu .close").on("click", function(e) {
        // $(".fullscreen-menu").css("bottom", "-100%");
        // $("body").removeClass("menuOpen");
        $("body").removeClass("menuOpen");
        setTimeout(function() {
            $("body").removeClass("menuOpened"); 
        }, 1);
    });

    $(window).scroll(function() {
        if( window.scrollY >= 60 ) {
            $("body").addClass("scrolled");
        } else {
            $("body").removeClass("scrolled");
        }
    });

    document.addEventListener( 'wpcf7invalid', function( event ) {
        var inputs = event.detail.inputs;
        var id = event.detail.contactFormId;
        if( id == 87 ) { // newsletter form
            var error = "";
            var fields = event.detail.apiResponse.invalidFields;
            for( var i = 0; i < fields.length; i++ ) {
                if( fields[i].message ) {
                    if( fields[i].message === "Required Field*" ) {
                        var field = fields[i].into;
                        var fieldName = "";
                        console.log(field);
                        if( field.indexOf("first-name") > -1 ) {
                            fieldName = "First Name";
                        } else if( field.indexOf("last-name") > -1 ) {
                            fieldName = "Last Name";
                        } else if( field.indexOf("email") > -1 ) {
                            fieldName = "Email Address";
                        } else if( field.indexOf("dobmonth") > -1 ) {
                            fieldName = "Month";
                        } else if( field.indexOf("dobyear") > -1 ) {
                            fieldName = "Year";
                        }
                        error += "<span>    - " + (fieldName ? fieldName : "") + " is a required field</span>";
                        continue;
                    }
                    error += "<span>    - " + fields[i].message + "</span>";
                }
            }
            console.log("after", error);
            if( error ) {
                setTimeout(function() {
                    jQuery(".wpcf7-validation-errors").html("Submission failed due to the following error(s):<br/>" + error);
                }, 10);
                
            }
            // for ( var i = 0; i < inputs.length; i++ ) {
                // if ( 'your-name' == inputs[i].name ) {
                // alert( inputs[i].value );
                // break;
            // }
            //wpcf7-validation-errors
        }
        
        
        
        
    }, false ); 

});