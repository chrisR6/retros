var gulp = require( 'gulp' );
// var browserSync = require( 'browser-sync' ).create();
var sass = require( 'gulp-sass' );
var uglify = require('gulp-uglify');
var include = require( 'gulp-include' );
gulp.task( 'sass', async function() {
    return gulp.src('./src/sass/style.scss')
            .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
            .pipe(gulp.dest('./dist/css/'));
} );

// Script compiling task

gulp.task("scripts", async function() {
    return gulp.src(['./src/js/script.js'])
        .pipe(include())
        // .pipe(babel())
        .pipe(uglify())
        .pipe( gulp.dest("./dist/js/") )});

// Browser Sync task

gulp.task( 'watch', function() {
    // browserSync.init( {
    //     files: [ './**/*.php', '*.php',  './dist/css/style.css', '/src/js/*.js', '/dist/js/*.js' ],
    //     proxy: url,
    // } );
    gulp.watch( './src/sass/*.scss', gulp.series('sass') );
    gulp.watch( './src/sass/**/*.scss', gulp.series('sass') );
    gulp.watch('./src/js/*.js', gulp.series('scripts') );
} );