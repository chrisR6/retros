<?php
/**
 * finnmccools functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package finnmccools
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'finnmccools_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function finnmccools_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on finnmccools, use a find and replace
		 * to change 'finnmccools' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'finnmccools', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'finnmccools' ),
				'footer' => esc_html__( 'Footer', 'finnmccools' ),
				'footer-right' => esc_html__( 'Footer - Right Menu', 'finnmccools' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		// add_theme_support(
		// 	'custom-background',
		// 	apply_filters(
		// 		'finnmccools_custom_background_args',
		// 		array(
		// 			'default-color' => 'ffffff',
		// 			'default-image' => '',
		// 		)
		// 	)
		// );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		// add_theme_support(
		// 	'custom-logo',
		// 	array(
		// 		'height'      => 250,
		// 		'width'       => 250,
		// 		'flex-width'  => true,
		// 		'flex-height' => true,
		// 	)
		// );
		if( function_exists('acf_add_options_page') ) { 
			acf_add_options_page(array(
				'page_title' 	=> 'Theme General Settings',
				'menu_title'	=> 'Theme Settings',
				'menu_slug' 	=> 'theme-general-settings',
				'capability'	=> 'edit_posts',
				'redirect'		=> false
			));
		}
	}
endif;
add_action( 'after_setup_theme', 'finnmccools_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function finnmccools_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'finnmccools_content_width', 640 );
}
add_action( 'after_setup_theme', 'finnmccools_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function finnmccools_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'finnmccools' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'finnmccools' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'finnmccools_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function finnmccools_scripts() {
	$cacheVersion = '20151215';
	wp_enqueue_style( 'finnmccools-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'finnmccools-style', 'rtl', 'replace' );

	wp_enqueue_script( 'finnmccools-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	wp_enqueue_script("jquery");
	// CountUp
	// wp_enqueue_script( 'CountUp', get_template_directory_uri() . '/js/countup.js', array(), '20151215', true );

	// Waypoints
	// wp_enqueue_script( 'Waypoints', get_template_directory_uri() . '/js/waypoints.min.js', array(), '20151215', true );
	// wp_enqueue_script( 'jquery', get_template_directory_uri() . '/dist/js/jquery-3.4.1.min.js', array(), '20151215', true );
	wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/dist/js/script.js', array('jquery'), '20151215', true );
	
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/dist/js/bootstrap.min.js', array('jquery'), '20151215', true );


	wp_enqueue_style( 'bootstrap-styles', get_template_directory_uri() . '/dist/css/bootstrap.min.css', array(), $cacheVersion );
	wp_enqueue_style( 'clc-styles', get_template_directory_uri() . '/dist/css/style.css', array(), $cacheVersion );

	// Featherlight
	wp_enqueue_script( 'featherlight', get_template_directory_uri() . '/dist/js/featherlight.min.js', array(), '20151215', true );
	wp_enqueue_style( 'featherlight-styles', get_template_directory_uri() . '/dist/css/featherlight.min.css', array(), $cacheVersion );

	wp_enqueue_script( 'googlemaps', "https://maps.googleapis.com/maps/api/js?key=AIzaSyAzjRz_i2OntIYrj-jZxzg-CqqE0NfUtzM", array(), '20151215', true );
	
	// Animate CSS & WoW JS
	wp_enqueue_script( 'wowjs', get_template_directory_uri() . '/dist/js/wow.min.js', array(), '20151215', true );
	wp_enqueue_style( 'animate-styles', get_template_directory_uri() . '/dist/css/animate.min.css', array(), $cacheVersion );
	// wp_enqueue_style( 'frankruhlibre', 'https://fonts.googleapis.com/css2?family=Frank+Ruhl+Libre:wght@300;400;500;700;900&display=swap', array(), $cacheVersion );
	// wp_enqueue_style( 'owsald', 'https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&display=swap', array(), $cacheVersion );
	wp_enqueue_style('fonts', "https://use.typekit.net/fhf0err.css", array(), $cacheVersion);
	// wp_enqueue_style( 'fonts', 'https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,400;1,500;1,600;1,700;1,800;1,900&display=swap', array(), $cacheVersion );


	wp_enqueue_script( 'select2script', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js', array(), '20151215', true );
	wp_enqueue_style( 'select2css', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css', array(), $cacheVersion );

	// if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	// 	wp_enqueue_script( 'comment-reply' );
	// }
}
add_action( 'wp_enqueue_scripts', 'finnmccools_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

require get_template_directory() . '/inc/wp-bootstrap-walker.php';



add_action( 'init', 'post_type_venues_init' );

function post_type_venues_init() {
	register_post_type( 'venue', array(
		'labels' => array(
			'name'               => 'Venues',
			'singular_name'      => 'Venue',
			'menu_name'          => 'Venues',
			'name_admin_bar'     => 'Venues',
			'all_items'          => 'All Venues',
			'add_new'            => 'Add New',
			'add_new_item'       => 'Add New Venue',
			'edit_item'          => 'Edit Venue',
			'new_item'           => 'New Venue',
			'view_item'          => 'View Venue',
			'search_items'       => 'Search Venues',
			'not_found'          => 'No Venues Found',
			'not_found_in_trash' => 'No Venues Found in Trash',
			'parent_item_colon'  => 'Parent Venue'
		),
		'public' => true,
		'menu_icon' =>'dashicons-admin-multisite',
		'capability_type' => 'page',
		'supports' => array(
            'title',
			'editor'
		),
		'has_archive' => false
	));
}
